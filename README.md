## Project overview
The current project is a Java flavor of [the test automation project](https://bitbucket.org/katarzyna_boguslawska/wsb-toa-page-object-python-2017) originally written in Python.
The project you're looking at is being written individually.
