package TrelloTesting.Objects.Base;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;


public class ElementStabilized implements ExpectedCondition<Boolean> {
    private By locator;
    private PageElement referenceElement;

    public ElementStabilized(By locator, PageElement referenceElement){
        this.locator = locator;
        this.referenceElement = referenceElement;
    }

    @Override
    public Boolean apply(WebDriver driver) {
        try {
            WebElement element = referenceElement.findElement(locator);
            Actions action = new Actions(driver);
            action.moveToElement(element).perform();
            return Boolean.TRUE;
        } catch (StaleElementReferenceException | NoSuchElementException exc){
            return Boolean.FALSE;
        }
    }

    @Override
    public boolean equals(Object o) {
        return false;
    }
}
