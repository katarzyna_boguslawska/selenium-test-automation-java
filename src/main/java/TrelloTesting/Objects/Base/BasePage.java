package TrelloTesting.Objects.Base;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class BasePage {
    private String url;
    private WebDriver driver;

    public BasePage(WebDriver driver){
        this.driver = driver;
        driver.manage().window().maximize();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void navigate(String url){
        driver.get(url);
    }

    public Boolean isPresent(By locator){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 3);
            wait.until(ExpectedConditions.presenceOfElementLocated(locator));
            return Boolean.TRUE;
        }
        catch (TimeoutException e){
            return Boolean.FALSE;
        }
    }

    public Boolean isVisible(By locator){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 3);
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            return Boolean.TRUE;
        }
        catch (TimeoutException e){
            return Boolean.FALSE;
        }
    }

    public Boolean isClickable(By locator){
        try {
            WebDriverWait wait = new WebDriverWait(driver, 3);
            wait.until(ExpectedConditions.elementToBeClickable(locator));
            return Boolean.TRUE;
        }
        catch (TimeoutException e){
            return Boolean.FALSE;
        }
    }

    public Boolean isAbsent(By locator){
        return !isPresent(locator);
    }

    public Boolean isAbsent(PageElement referenceElement, By locator){
        int maxAttempt = (int)(3/0.2);
        int attempt = 0;
        slowDown(200);
        while (attempt < maxAttempt){
            if (referenceElement.findElements(locator).size() == 0){
                return Boolean.TRUE;
            } else {
                attempt++;
                slowDown(200);
            }
        }
        return Boolean.FALSE;
    }

    public Boolean isAbsent(By referenceElementLocator, By locator){
        PageElement referenceElement = getElement(referenceElementLocator);
        int maxAttempt = (int)(3/0.2);
        int attempt = 0;
        slowDown(200);
        while (attempt < maxAttempt){
            if (referenceElement.findElements(locator).size() == 0){
                return Boolean.TRUE;
            } else {
                attempt++;
                slowDown(200);
            }
        }
        return Boolean.FALSE;
    }

    public PageElement getElement(By locator){
        WebDriverWait wait = new WebDriverWait(driver, 3);
        WebElement root = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return new PageElement(locator, driver, root);
    }

    public PageElement getElement(PageElement parent, By locator){
        WebDriverWait wait = new WebDriverWait(driver, 3);
        if (wait.until(new RelativeElementPresent(locator, parent.getLocator()))){
            WebElement root = parent.findElement(locator);
            return new PageElement(locator, driver, root);
        } else{
            throw new NoSuchElementException();
        }
    }

    public PageElement getElement(By parent, By locator){
        PageElement parentElement = getElement(parent);
        WebDriverWait wait = new WebDriverWait(driver, 3);
        if (wait.until(new RelativeElementPresent(locator, parent))){
            WebElement root = parentElement.findElement(locator);
            return new PageElement(locator, driver, root);
        } else{
            throw new NoSuchElementException();
        }
    }

    public List<PageElement> getElements(By locator){
        if (isVisible(locator)) {
            List<PageElement> elements = new ArrayList<>();
            List<WebElement> webElements = driver.findElements(locator);
            for (WebElement webElement : webElements){
                PageElement element = new PageElement(locator, driver, webElement);
                elements.add(element);
            }
            return elements;
        } else {
            throw new NoSuchElementException();
        }
    }

    public List<PageElement> getElements(PageElement referenceElement, By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 3);
        if (wait.until(new RelativeElementPresent(locator, referenceElement.getLocator()))) {
            List<PageElement> elements = new ArrayList<>();
            List<WebElement> webElements = referenceElement.findElements(locator);
            for (WebElement webElement : webElements) {
                PageElement element = new PageElement(locator, driver, webElement);
                elements.add(element);
            }
            return elements;
        } else {
            throw new NoSuchElementException();
        }
    }

    public List<PageElement> getElements(By referenceElementLocator, By locator) {
        PageElement referenceElement = getElement(referenceElementLocator);
        WebDriverWait wait = new WebDriverWait(driver, 3);
        if (wait.until(new RelativeElementPresent(locator, referenceElementLocator))) {
            List<PageElement> elements = new ArrayList<>();
            List<WebElement> webElements = referenceElement.findElements(locator);
            for (WebElement webElement : webElements) {
                PageElement element = new PageElement(locator, driver, webElement);
                elements.add(element);
            }
            return elements;
        } else {
            throw new NoSuchElementException();
        }
    }

    public void slowDown(int howMuch){
        try {
            Thread.sleep(howMuch);                 //1000 milliseconds is one second.
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public void validateElement(PageElement element){
        if (element == null){
            throw new NoSuchElementException();
        }
    }

    public void goBack(){
        driver.navigate().back();
    }

    public PageElement scrollTo(By locator){
        PageElement element = getElement(locator);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element.getRoot());
        return element;
    }

    public PageElement scrollTo(PageElement element){
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element.getRoot());
        return element;
    }
}
