package TrelloTesting.Objects.Base;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class PageElement extends BasePage implements WebElement {

    private WebElement root;
    private By locator;
    private WebDriver driver;

    public PageElement(By locator, WebDriver driver) {
        super(driver);
        this.locator = locator;
        this.driver = driver;
        this.root = getElement(locator);
    }

    public PageElement(By locator, WebDriver driver, WebElement element) {
        super(driver);
        this.locator = locator;
        this.driver = driver;
        this.root = element;
    }

    public By getLocator() {
        return locator;
    }

    public void setLocator(By locator) {
        this.locator = locator;
    }

    public WebElement getRoot() {
        return root;
    }

    public void setRoot(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 3);
        this.root = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public PageElement dragTo(By locator){
        Actions action = new Actions(driver);
        WebElement destination = driver.findElement(locator);
        action.moveToElement(root).dragAndDrop(root, destination).perform();
        return this;
    }

    public PageElement dragTo(WebElement destination){
        Actions action = new Actions(driver);
        action.moveToElement(root).dragAndDrop(root, destination).perform();
        return this;
    }

    public PageElement focusAndType(CharSequence... text){
        clear();
        Actions action = new Actions(driver);
        action.moveToElement(root).click().sendKeys(text).perform();
        return this;
    }

    public PageElement clickEnter(){
        root.sendKeys(Keys.ENTER);
        return this;
    }

    public PageElement type(CharSequence... text){
        clear();
        Actions action = new Actions(driver);
        action.moveToElement(root).sendKeys(text).perform();
        return this;
    }
    @Override
    public void clear(){
        root.clear();
    }

    @Override
    public void click(){
        Actions action = new Actions(driver);
        action.moveToElement(root).click().perform();
    }

    @Override
    public void submit() {
        root.submit();
    }

    @Override
    public void sendKeys(CharSequence... charSequences) {
        root.sendKeys();
    }

    @Override
    public String getTagName() {
        return root.getTagName();
    }

    @Override
    public String getAttribute(String s) {
        return root.getAttribute(s);
    }

    @Override
    public boolean isSelected() {
        return root.isSelected();
    }

    @Override
    public boolean isEnabled() {
        return root.isEnabled();
    }

    @Override
    public String getText() {
        return root.getText();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return root.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return root.findElement(by);
    }

    @Override
    public boolean isDisplayed() {
        return root.isDisplayed();
    }

    @Override
    public Point getLocation() {
        return root.getLocation();
    }

    @Override
    public Dimension getSize() {
        return root.getSize();
    }

    @Override
    public Rectangle getRect() {
        return root.getRect();
    }

    @Override
    public String getCssValue(String s) {
        return root.getCssValue(s);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return null;
    }
}
