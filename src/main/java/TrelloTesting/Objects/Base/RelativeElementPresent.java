package TrelloTesting.Objects.Base;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class RelativeElementPresent implements ExpectedCondition<Boolean> {
    private By locator;
    private By referenceElementLocator;

    public RelativeElementPresent(By locator, By referenceElementLocator){
        this.locator = locator;
        this.referenceElementLocator = referenceElementLocator;
    }

    @Override
    public Boolean apply(WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 3);
            WebElement referenceElement = wait.until(ExpectedConditions.visibilityOfElementLocated(referenceElementLocator));
            WebElement element = referenceElement.findElement(locator);
            return Boolean.TRUE;
        } catch (StaleElementReferenceException | NoSuchElementException exc){
            return Boolean.FALSE;
        }
    }

    @Override
    public boolean equals(Object o) {
        return false;
    }
}
