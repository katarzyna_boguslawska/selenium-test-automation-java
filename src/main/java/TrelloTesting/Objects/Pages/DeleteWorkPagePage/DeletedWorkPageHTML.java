package TrelloTesting.Objects.Pages.DeleteWorkPagePage;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class DeletedWorkPageHTML extends BasePage{

    private final By notFoundLocator = By.xpath("//h1[text()='Board not found.']");
    private WebDriver driver;

    public DeletedWorkPageHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getNotFoundLocator() {
        return notFoundLocator;
    }

    public PageElement getNotFound(){
        return getElement(getNotFoundLocator());
    }
}
