package TrelloTesting.Objects.Pages.DeleteWorkPagePage;


import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import org.openqa.selenium.WebDriver;

public class DeletedWorkPagePage extends LoggedInPage {

    private WebDriver driver;
    private DeletedWorkPageHTML html;


    public DeletedWorkPagePage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new DeletedWorkPageHTML(driver);
    }

    public Boolean isDeleted(){
        return isPresent(html.getNotFoundLocator());
    }
}
