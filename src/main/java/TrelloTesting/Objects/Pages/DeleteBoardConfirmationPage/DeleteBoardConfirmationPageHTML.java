package TrelloTesting.Objects.Pages.DeleteBoardConfirmationPage;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class DeleteBoardConfirmationPageHTML extends BasePage{

    private final By deleteLocator = By.xpath("//a[@class='quiet js-delete']");
    private final By reopenLocator = By.xpath("//a[@class='js-reopen']");
    private WebDriver driver;

    public DeleteBoardConfirmationPageHTML(WebDriver driver){
        super(driver);
        this.driver = driver;
    }

    public By getDeleteLocator() {
        return deleteLocator;
    }

    public By getReopenLocator() {
        return reopenLocator;
    }

    public PageElement getDeleteLink() {
        return getElement(getDeleteLocator());
    }

    public PageElement getReopenLink() {
        return getElement(getReopenLocator());
    }
}
