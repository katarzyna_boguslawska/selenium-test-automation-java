package TrelloTesting.Objects.Pages.DeleteBoardConfirmationPage;


import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Delete.DeleteBoardPopup;
import TrelloTesting.Objects.Pages.DeleteWorkPagePage.DeletedWorkPagePage;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import org.openqa.selenium.WebDriver;

public class DeleteBoardConfirmationPage extends LoggedInPage {


    private WebDriver driver;
    private DeleteBoardConfirmationPageHTML html;

    public DeleteBoardConfirmationPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new DeleteBoardConfirmationPageHTML(driver);
    }

    private DeleteBoardPopup openFinalDeletingMenu(){
        html.getDeleteLink().click();
        return new DeleteBoardPopup(driver);

    }
    public DeletedWorkPagePage confirmClosing(){
        DeleteBoardPopup popup = openFinalDeletingMenu();
        return popup.permanentlyDelete();
    }

    public void reopen(){
        html.getReopenLink().click();
    }
}
