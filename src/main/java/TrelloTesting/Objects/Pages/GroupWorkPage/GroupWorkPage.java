package TrelloTesting.Objects.Pages.GroupWorkPage;

import TrelloTesting.FixedChoices.Colors;
import TrelloTesting.Interfaces.WorkPageable;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.List;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.BoardMenu;
import TrelloTesting.Objects.Pages.DeleteWorkPagePage.DeletedWorkPagePage;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import TrelloTesting.Objects.Pages.CommonHTML.WorkPageHTML;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.*;

public class GroupWorkPage extends LoggedInPage implements WorkPageable{

    public java.util.List<List> lists = new ArrayList<>();
    private WebDriver driver;
    public BoardMenu menu;
    public String title;
    private String workspace;
    private WorkPageHTML html;

    public GroupWorkPage(WebDriver driver, String title, String workspace){
        super(driver);
        this.driver = driver;
        this.title = title;
        this.workspace = workspace;
        this.html = new WorkPageHTML(driver, title);
    }

    private List makeList(String listTitle){
        html.getListTitleInput().type(listTitle);
        html.getSaveListButton().click();
        By parent = getParent(listTitle);
        return new List(listTitle, parent, driver);
    }

    private By getParent(String listTitle){
        return html.getListParentLocator(listTitle);
    }

    public void addList(String listTitle){
        List list = makeList(listTitle);
        lists.add(list);
    }

    public List findListByTitle(String listTitle){
        for(List list : lists){
            if (list.title.equals(listTitle)){
                return list;
            }
        }
        return null;
    }

    public void archiveListWithTitle(String listTitle){
        List listToArchive = findListByTitle(listTitle);
        listToArchive.archive();
        lists.remove(listToArchive);
    }

    public int countLists(){
        return getElements(html.getGenericListLocator()).size();
    }

    @Override
    public BoardMenu showBoardMenu() {
        if(isAbsent(html.getCloseMenuLocator())){
            html.getShowMenuLink().click();
            menu = new BoardMenu(driver);
            return menu;
        }
        if (menu != null){
            return menu;
        } else {
            menu = new BoardMenu(driver);
            return menu;
        }
    }

    public DeletedWorkPagePage deleteBoard(){
        BoardMenu menu = showBoardMenu();
        return menu.deleteBoard();
    }

    public void renameBoard(String newName){
        html.getTitleLink().click();
        html.getNewNameInput().type(newName);
        html.getNameChangeButton().click();
    }

    public Boolean isTitled(String name){
        return isPresent(html.getWorkPageTitleLocator(name));
    }

    public Boolean isBackgroundColored(Colors color){
        String backgroundColor = html.getBody().getCssValue("background-color");
        return backgroundColor.equals(color.getRGBcomponents());
    }

    public Boolean isBackgroundImage(){
        String backgroundImage = getBackgroundPattern();
        return backgroundImage.startsWith("url(\"https");
    }

    private String getBackgroundPattern(){
        int maxAttempt = (int)(5/0.5);
        int attempt = 0;
        slowDown(500);
        while (attempt < maxAttempt){
            if (html.getBody().getCssValue("background-image").equals("none")){
                attempt++;
                slowDown(500);
            } else {
                return html.getBody().getCssValue("background-image");
            }
        }
        return "none";
    }

}
