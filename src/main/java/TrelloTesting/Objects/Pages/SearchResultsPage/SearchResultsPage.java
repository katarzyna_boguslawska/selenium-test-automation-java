package TrelloTesting.Objects.Pages.SearchResultsPage;


import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchResultsPage extends LoggedInPage {
    private WebDriver driver;
    public String searchPhrase;
    public By result;

    public SearchResultsPage(WebDriver driver, String searchPhrase){
        super(driver);
        this.driver = driver;
        this.searchPhrase = searchPhrase;
        this.result = By.xpath("//a[@class='js-open-board compact-board-tile-link dark' and .//span[@title='" + this.searchPhrase + "']]");
    }

    public Boolean isResultPresent(){
        return isPresent(result);
    }
}
