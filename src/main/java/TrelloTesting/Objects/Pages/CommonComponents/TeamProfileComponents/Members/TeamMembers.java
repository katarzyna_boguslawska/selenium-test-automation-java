package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Members;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Members.Popups.MemberPopup;
import org.openqa.selenium.WebDriver;

public class TeamMembers extends BasePage{

    private WebDriver driver;
    private TeamMembersHTML html;

    public TeamMembers(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new TeamMembersHTML(driver);
    }

    private MemberPopup openMemberPopup(){
        html.getAddByNameButton().clickEnter();
        return new MemberPopup(driver);
    }

    public void addMember(String member){
        MemberPopup popup = openMemberPopup();
        popup.addMember(member);
    }

    public int countMembers(){
        return getElements(html.getGenericMember()).size();
    }

    public Boolean checkIfMembersNumberIncreased(int greaterThanWhat){
        int attemptsNo = (int)(3/0.2);
        for (int i = 0; i <= attemptsNo; i++){
            if (countMembers() > greaterThanWhat){
                return Boolean.TRUE;
            } else {
                slowDown(200);
            }
        }
        return Boolean.FALSE;
    }
}
