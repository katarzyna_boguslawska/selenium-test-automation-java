package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Members;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class TeamMembersHTML extends BasePage{

    private final By addByNameLocator = By.xpath("//a[@class='primary button-link autowrap' and .//span[text()='Add by Name or Email']]");
    private final By genericMember = By.cssSelector(".member-list-item-detail");
    private WebDriver driver;

    public TeamMembersHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getAddByNameLocator() {
        return addByNameLocator;
    }

    public By getGenericMember() {
        return genericMember;
    }

    public PageElement getAddByNameButton() {
        return getElement(getAddByNameLocator());
    }

    public PageElement getGenericMemberField() {
        return getElement(getGenericMember());
    }
}
