package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Visibility;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class VisibilityPopupHTML extends BasePage{

    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private WebDriver driver;

    public VisibilityPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public PageElement getCloseButton(){return getElement(getCloseLocator());}
}
