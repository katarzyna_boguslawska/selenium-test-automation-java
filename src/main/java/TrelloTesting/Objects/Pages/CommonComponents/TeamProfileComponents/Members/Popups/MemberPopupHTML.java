package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Members.Popups;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class MemberPopupHTML extends BasePage{

    private static final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private static final By nameInput = By.xpath("//input[@class='js-search-input js-autofocus' and @type='text']");
    private static final By sendInvitationLocator = By.xpath("//input[@class='wide primary js-send-email-invite' and @type='submit']");
    private WebDriver driver;

    public MemberPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getNameInputLocator() {
        return nameInput;
    }

    public By getSendInvitationLocator() {
        return sendInvitationLocator;
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getNameInput() {
        return getElement(getNameInputLocator());
    }

    public PageElement getSendInvitationButton() { return getElement(getSendInvitationLocator()); }
}
