package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Members.Popups;

import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

public class MemberPopup extends BasePage implements Closeable{

    private WebDriver driver;
    private MemberPopupHTML html;

    public MemberPopup(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new MemberPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    public void addMember(String member){
        PageElement memberNameInput = html.getNameInput();
        memberNameInput.type(member);
        try{
            html.getSendInvitationButton().click();
        } catch (TimeoutException e){
            memberNameInput.clickEnter();
        }
    }
}
