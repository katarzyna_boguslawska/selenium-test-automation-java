package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Settings;


import TrelloTesting.FixedChoices.Visibilities;
import TrelloTesting.Interfaces.Deletable;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Delete.DeletionPopup;
import TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Visibility.VisibilityPopup;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

public class TeamSettings extends BasePage implements Deletable{

    private String teamName;
    private WebDriver driver;
    private TeamSettingsHTML html;

    public TeamSettings(WebDriver driver, String teamName) {
        super(driver);
        this.driver = driver;
        this.teamName = teamName;
        this.html = new TeamSettingsHTML(driver);
    }

    public Visibilities getCurrentVisibility(){
        try {
            getElement(Visibilities.PRIVATE.getCurrentVisibilityLocator());
            return Visibilities.PRIVATE;
        } catch (TimeoutException e){
            return Visibilities.PUBLIC;
        }
    }

    private DeletionPopup openDeletingPopup(){
        scrollTo(html.getDeletingTeamLocator());
        html.getDeletingTeamLink().click();
        return new DeletionPopup(driver);
    }

    @Override
    public void delete() {
        DeletionPopup popup = openDeletingPopup();
        popup.delete();
    }

    private VisibilityPopup openVisibilityPopup(){
        html.getChangeVisibilityButton().click();
        return new VisibilityPopup(driver);
    }

    public void changeVisibility(Visibilities requestedVisibility){
        VisibilityPopup popup = openVisibilityPopup();
        popup.changeVisibility(requestedVisibility);
    }
}
