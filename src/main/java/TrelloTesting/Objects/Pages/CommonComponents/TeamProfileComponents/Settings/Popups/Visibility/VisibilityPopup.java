package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Visibility;


import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.FixedChoices.Visibilities;
import org.openqa.selenium.WebDriver;

public class VisibilityPopup extends BasePage implements Closeable{

    private WebDriver driver;
    private VisibilityPopupHTML html;

    public VisibilityPopup(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new VisibilityPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    public void changeVisibility(Visibilities requestedVisibility){
        getElement(requestedVisibility.getVisibilityButtonLocator()).click();
    }
}
