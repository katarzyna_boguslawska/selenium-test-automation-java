package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Boards;


import TrelloTesting.Objects.Base.BasePage;
import org.openqa.selenium.WebDriver;

public class TeamBoards extends BasePage{

    private WebDriver driver;
    private TeamBoardsHTML html;

    public TeamBoards(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new TeamBoardsHTML(driver);
    }
}
