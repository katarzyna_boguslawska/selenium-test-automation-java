package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Boards;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class TeamBoardsHTML extends BasePage {

    private final By createBoardLocator = By.xpath(".//a[@class='board-tile mod-add' and @href='#']");
    private WebDriver driver;

    public TeamBoardsHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCreateBoardLocator() {
        return createBoardLocator;
    }

    public PageElement getCreateBoardButton() {
        return getElement(getCreateBoardLocator());
    }
}
