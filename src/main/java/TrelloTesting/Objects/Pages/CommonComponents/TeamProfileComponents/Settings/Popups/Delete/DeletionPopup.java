package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Delete;


import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Interfaces.Deletable;
import TrelloTesting.Objects.Base.BasePage;
import org.openqa.selenium.WebDriver;

public class DeletionPopup extends BasePage implements Closeable, Deletable{

    private WebDriver driver;
    private DeletionPopupHTML html;

    public DeletionPopup(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new DeletionPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    @Override
    public void delete() {
        html.getConfirmDeletingButton().click();
    }
}
