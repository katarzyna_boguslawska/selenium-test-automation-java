package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Delete;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class DeletionPopupHTML extends BasePage{

    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By confirmDeletingLocator = By.xpath("//input[@class='js-confirm full negate' and @type='submit']");
    private WebDriver driver;

    public DeletionPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getConfirmDeletingLocator() {
        return confirmDeletingLocator;
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getConfirmDeletingButton() {
        return getElement(getConfirmDeletingLocator());
    }
}
