package TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Settings;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class TeamSettingsHTML extends BasePage{

    private final By changeVisibilityLocator = By.xpath("//a[@class='button-link u-text-align-center' and .//span[text()='Change']]");
    private final By deletingTeamLocator = By.xpath("//a[@class='quiet-button' and .//span[text()='Delete this team?']]");
    private WebDriver driver;

    public TeamSettingsHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getChangeVisibilityLocator() {
        return changeVisibilityLocator;
    }

    public By getDeletingTeamLocator() {
        return deletingTeamLocator;
    }

    public PageElement getChangeVisibilityButton() {
        return getElement(getChangeVisibilityLocator());
    }

    public PageElement getDeletingTeamLink() {
        return getElement(getDeletingTeamLocator());
    }
}
