package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Close;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class CloseBoardPopupHTML extends BasePage{

    private final By closeBoardButtonLocator = By.xpath("//input[@class='js-confirm full negate' and @type='submit']");
    private final By closePopupLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private WebDriver driver;

    public CloseBoardPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseBoardLocator() {
        return closeBoardButtonLocator;
    }

    public By getClosePopupLocator() {
        return closePopupLocator;
    }

    public PageElement getCloseBoardButton() {
        return getElement(getCloseBoardLocator());
    }
    public PageElement getClosePopupButton() { return getElement(getClosePopupLocator()); }
}
