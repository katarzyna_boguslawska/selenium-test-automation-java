package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class CardMenuHTML extends BasePage{

    private static By windowLocator = By.xpath("//div[@class='window']");
    private static By membersLocator = By.xpath("//a[@class='button-link js-change-card-members']");
    private static By labelsLocator = By.xpath("//a[@class='button-link js-edit-labels']");
    private static By checklistLocator = By.xpath("//a[@class='button-link js-add-checklist-menu']");
    private static By dueDateLocator = By.xpath("//a[@class='button-link js-add-due-date']");
    private static By attachmentLocator = By.xpath("//a[@class='button-link js-attach']");
    private static By archiveLocator = By.xpath("//a[@class='button-link js-archive-card']");
    private static By shareAndMoreLocator = By.xpath("//a[@class='quiet-button js-more-menu']");
    private static By closeLocator = By.xpath("//a[@class='icon-lg icon-close dialog-close-button js-close-window']");
    private static By closeLocatorOnCover = By.xpath("//a[@class='icon-lg icon-close dialog-close-button js-close-window dialog-close-button-light']");
    private static By genericChecklistLocator = By.xpath("//div[@class='checklist']");
    private WebDriver driver;
    
    //comment submenu locators
    private static By commentLocator = By.xpath(".//textarea[@class='comment-box-input js-new-comment-input']");
    private static By emojiLocator = By.xpath(".//a[@class='comment-box-options-item js-comment-add-emoji']");
    private static By submitCommentLocator = By.xpath(".//input[@class='primary confirm mod-no-top-bottom-margin js-add-comment']");

    public CardMenuHTML(WebDriver driver){
        super(driver);
        this.driver = driver;
    }

    public By getWindowLocator() {
        return windowLocator;
    }

    public By getMembersLocator() {
        return membersLocator;
    }

    public By getLabelsLocator() {
        return labelsLocator;
    }

    public By getChecklistLocator() {
        return checklistLocator;
    }

    public By getDueDateLocator() {
        return dueDateLocator;
    }

    public By getAttachmentLocator() {
        return attachmentLocator;
    }

    public By getArchiveLocator() {
        return archiveLocator;
    }

    public By getShareAndMoreLocator() {
        return shareAndMoreLocator;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getCloseLocatorOnCover() {
        return closeLocatorOnCover;
    }

    public By getGenericChecklistLocator() {
        return genericChecklistLocator;
    }

    public By getCommentLocator() {
        return commentLocator;
    }

    public By getEmojiLocator() {
        return emojiLocator;
    }

    public By getSubmitCommentLocator() {
        return submitCommentLocator;
    }

    public By getAttachmentContainerLocator(String filename){
        return By.xpath("//div[@class='u-clearfix js-attachment-list ui-sortable' and .//a[@class='attachment-thumbnail-preview js-open-viewer attachment-thumbnail-preview-is-cover' and contains(@style, '" + filename + "')]]");
    }

    public By getChecklistContainerLocator(String title){
        return By.xpath("//div[@class='checklist' and .//h3[.='" + title + "']]");
    }

    public By getCommentContainerLocator(String commentText){
        return By.xpath(".//div[@class='phenom mod-comment-type' and .//p[starts-with(text(), '" + commentText + "')]]");
    }

    public By getCommentTextareaLocator(String commentText){
        return By.xpath("//p[text()='" + commentText+ "']");
    }

    public PageElement getWindow() {
        return getElement(getWindowLocator());
    }

    public PageElement getMembersButton() {
        return getElement(getMembersLocator());
    }

    public PageElement getLabelsButton() {
        return getElement(getLabelsLocator());
    }

    public PageElement getChecklistButton() {
        return getElement(getChecklistLocator());
    }

    public PageElement getDueDateButton() {
        return getElement(getDueDateLocator());
    }

    public PageElement getAttachmentButton() {
        return getElement(getAttachmentLocator());
    }

    public PageElement getArchiveButton() {
        return getElement(getArchiveLocator());
    }

    public PageElement getShareAndMoreLink() {
        return getElement(getShareAndMoreLocator());
    }

    public PageElement getClose() {
        return getElement(getCloseLocator());
    }

    public PageElement getCloseOnCover() {
        return getElement(getCloseLocatorOnCover());
    }

    public PageElement getGenericChecklist() {
        return getElement(getGenericChecklistLocator());
    }

    public PageElement getComment() {
        return getElement(getCommentLocator());
    }

    public PageElement getEmojiOption() {
        return getElement(getEmojiLocator());
    }

    public PageElement getSubmitCommentButton() {
        return getElement(getSubmitCommentLocator());
    }
}
