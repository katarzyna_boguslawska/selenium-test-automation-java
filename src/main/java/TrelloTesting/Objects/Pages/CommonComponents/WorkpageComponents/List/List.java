package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List;

import TrelloTesting.FixedChoices.Colors;
import TrelloTesting.FixedChoices.Emojis;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Card;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist.Checklist;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.CardMenu;
import TrelloTesting.Objects.Pages.JsonExportedPage.JsonExportedPage;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;


public class List extends BasePage {

    public String title;
    private By parent;
    private java.util.List<Card> cards;
    private WebDriver driver;
    private ListHTML html;

    public List(String title, By parent, WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.parent = parent;
        this.title = title;
        this.cards = new ArrayList<Card>();
        this.html = new ListHTML(driver);
    }

    private PageElement refreshListParent(){
        java.util.List<PageElement> allListContainers = getElements(html.getListParentNodesLocator());
        for (PageElement container : allListContainers){
            PageElement child = getElement(container, html.getListHeaderLocator(title));
            if (child != null){
                return container;
            }
        }
        return null;
    }

    public void archive(){
        try{
            getElement(parent, html.getMoreMenuLocator()).click();
        } catch (StaleElementReferenceException e){
            PageElement newParent = refreshListParent();
            getElement(newParent, html.getMoreMenuLocator()).click();
        }
        html.getArchiveListButton().click();
    }

    private By getCardContainer(String textOnCard){
        //PageElement parent = getElement(By.xpath("//div[@class='list-card js-member-droppable ui-droppable active-card' and .//a[text()='" + textOnCard + "']]"));
        return html.getCardParentLocator(textOnCard);
    }


    private Card makeCard(String textOnCard){
        getElement(parent, html.getAddCardLocator()).click();
        getElement(parent, html.getCardTextLocator()).type(textOnCard);
        getElement(parent, html.getAddLocator()).click();
        getElement(parent, html.getQuitAddingLocator()).click();
        By parent = getCardContainer(textOnCard);
        return new Card(driver, parent, textOnCard);
    }

    public void addCard(String textOnCard){
        Card card = makeCard(textOnCard);
        cards.add(card);
    }

    public Card findCardByText(String cardText){
        for (Card card : cards){
            if(card.text.equals(cardText)){
                return card;
            }
        }
        return null;
    }

    public void addDueDateOnCardWithText(String cardText, String date){
        findCardByText(cardText).showCardMenu().addDueDate(date);
    }

    public void addAttachmentFromLinkToCardWithText(String cardText, String link){
        findCardByText(cardText).showCardMenu().addAttachmentFromLink(link);
    }

    public void archiveCardWithText(String cardText){
        Card card = findCardByText(cardText);
        card.showCardMenu().archiveCard();
        cards.remove(card);
    }

    public JsonExportedPage exportToJsonCardWithText(String cardText){
        return findCardByText(cardText).showCardMenu().exportToJson();
    }

    public void addChecklistToCardWithText(String cardText, String checklistTitle){
        findCardByText(cardText).showCardMenu().addChecklist(checklistTitle);
    }

    public void addChecklistToCardWithText(String cardText, String checklistTitle, String taskName){
        findCardByText(cardText).showCardMenu().addChecklist(checklistTitle);
        findCardByText(cardText).showCardMenu(Boolean.TRUE).populateChecklist(checklistTitle, taskName);
    }

    public void completeListedTasksOnCardWithText(String cardText, String checklistTitle, String [] tasks){
        Checklist checklist = findCardByText(cardText).menu.findChecklistByTitle(checklistTitle);
        for (String task : tasks){
            checklist.completeTask(task);
        }
    }

    public void dragCardWithTextInPlaceOfCardWithText(String sourceText, String destinationText, List destinationList){
        By destination = destinationList.findCardByText(destinationText).stableParent;
        findCardByText(sourceText).dragAndDropCard(destination);
        cards.remove(findCardByText(sourceText));
    }

    public void labelCardWithText(String cardText, Colors color){
        findCardByText(cardText).showCardMenu().label(color);
    }

    public void assignMemberToCardWithText(String cardText, String who){
        findCardByText(cardText).showCardMenu().addMember(who);
    }

    public void commentCardWithText(String cardText, String commentText){
        findCardByText(cardText).showCardMenu().comment(commentText);
    }

    public void commentCardWithTextAndAddEmoji(String cardText, String commentText, Emojis emoji){
        findCardByText(cardText).showCardMenu().comment(commentText, emoji);
    }

    public void deleteCommentFromCardWithText(String cardText, String commentText){
        findCardByText(cardText).showCardMenu().deleteCommentWithContent(commentText);
    }

    public Boolean wasDueDateAddedToCardWithText(String cardText){
        return findCardByText(cardText).dueDateWasAdded();
    }

    public Boolean wasCardWithTextArchived(String cardText){
        return findCardByText(cardText) != null ? Boolean.FALSE : Boolean.TRUE;
    }

    public Boolean wasAttachmentAddedToCardWithText(String cardText){
        return findCardByText(cardText).attachmentWasAdded();
    }

    public Boolean wasCardWithTextDroppedInPlaceOfCardWithText(String sourceCardText, List destinationList){
        Card dragged;
        try {
            dragged = findCardByText(sourceCardText);
        } catch (StaleElementReferenceException e){
            dragged = null;
        }
        PageElement droppedParent = getElement(destinationList.parent);
        PageElement dropped = getElement(droppedParent, By.xpath(".//*[text()='" + sourceCardText + "']"));
        return (dragged == null) && (dropped != null) ? Boolean.TRUE : Boolean.FALSE;
    }

    public Boolean wasCardWithTextLabelled(String cardText, Colors color){
        return findCardByText(cardText).hasLabelOfColor(color);
    }

    public Boolean wasCardWithTextAssignedTo(String cardText, String member){
        return findCardByText(cardText).MemberWasAdded(member);
    }

    public Boolean wasCardWithTextCommented(String cardText, String commentText){
        Card card = findCardByText(cardText);
        CardMenu menu = card.showCardMenu(Boolean.TRUE);
        return menu.wasCommentAdded(commentText);
    }

    public Boolean wasCardWithTextCommented(String cardText, String commentText, Emojis emoji){
        Card card = findCardByText(cardText);
        CardMenu menu = card.showCardMenu(Boolean.TRUE);
        Boolean isEmoji = menu.findCommentByText(commentText, Boolean.TRUE).checkIfHasEmoji(emoji);
        menu.hideMenu();
        return isEmoji;
    }

    public Boolean wasCommentRemovedFromCardWithText(String cardText, String commentText){
        CardMenu menu = findCardByText(cardText).showCardMenu();
        return menu.isCommentRemoved(commentText);
    }

    public Boolean wasChecklistAddedToCardWithText(String cardText, String checklistTitle){
        Card card = findCardByText(cardText);
        Boolean was_added =  card.showCardMenu(Boolean.TRUE).wasChecklistAdded(checklistTitle);
        card.showCardMenu(Boolean.TRUE).removeChecklist(checklistTitle);
        return was_added;
    }

    public Boolean wasChecklistPopulatedOnCardWithText(String cardText, String checklistTitle){
        Card card = findCardByText(cardText);
        Boolean was_populated = card.showCardMenu(Boolean.TRUE).wasChecklistWithTitlePopulated(checklistTitle, 4);
        card.showCardMenu(Boolean.TRUE).removeChecklist(checklistTitle);
        return was_populated;
    }

    public Boolean wereTasksCompleted(String cardText, String checklistTitle, int howManyWereCompleted){
        double completedTasksNumber = (double)howManyWereCompleted;
        Card card = findCardByText(cardText);
        Boolean were_completed = card.showCardMenu(Boolean.TRUE).wereTasksCompleted(checklistTitle, completedTasksNumber);
        card.showCardMenu(Boolean.TRUE).removeChecklist(checklistTitle);
        return were_completed;
    }
}
