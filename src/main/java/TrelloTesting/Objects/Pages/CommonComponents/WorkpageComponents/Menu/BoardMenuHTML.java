package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class BoardMenuHTML extends BasePage{

    private final By archivedItemsLocator = By.xpath("//a[@class='board-menu-navigation-item-link js-open-archive']");
    private final By closeMenuLocator = By.xpath("//a[@class='board-menu-header-close-button icon-lg icon-close js-hide-sidebar']");
    private WebDriver driver;

    public BoardMenuHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getArchivedItemsLocator() {
        return archivedItemsLocator;
    }

    public By getCloseMenuLocator() {
        return closeMenuLocator;
    }

    public PageElement getArchivedItemsButton() {
        return getElement(getArchivedItemsLocator());
    }

    public PageElement getCloseMenuButton() {
        return getElement(getCloseMenuLocator());
    }
}
