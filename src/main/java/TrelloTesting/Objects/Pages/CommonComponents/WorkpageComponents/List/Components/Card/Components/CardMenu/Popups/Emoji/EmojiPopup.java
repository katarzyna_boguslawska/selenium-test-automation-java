package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Emoji;


import TrelloTesting.FixedChoices.Emojis;
import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Base.BasePage;
import org.openqa.selenium.WebDriver;


public class EmojiPopup extends BasePage implements Closeable{

    private WebDriver driver;
    private EmojiPopupHTML html;

    public EmojiPopup(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new EmojiPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().clickEnter();
    }

    public void insertEmoji(Emojis emoji){
        getElement(emoji.getEmojiLocator()).click();
    }
}
