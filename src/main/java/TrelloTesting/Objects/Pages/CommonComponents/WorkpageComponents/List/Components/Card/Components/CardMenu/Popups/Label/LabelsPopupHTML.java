package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Label;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class LabelsPopupHTML extends BasePage{

    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By windowLocator = By.xpath("//div[@class='pop-over is-shown']");
    private final By labelInputLocator = By.xpath("//input[@class='js-autofocus js-label-search']");
    private final By createNewLabelLocator = By.xpath("//a[@class='quiet-button full js-add-label']");
    private final By colorBlindModeLocator = By.xpath("//a[@class='quiet-button full js-toggle-color-blind-mode']");
    private final By selectedLabelTickLocator = By.xpath("//span[@class='icon-sm icon-check card-label-selectable-icon light']");
    private final By editLabelLocator = By.xpath("//a[@class='card-label-edit-button icon-sm icon-edit js-edit-label']");
    private WebDriver driver;

    public LabelsPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getWindowLocator() {
        return windowLocator;
    }

    public By getLabelInputLocator() {
        return labelInputLocator;
    }

    public By getCreateNewLabelLocator() {
        return createNewLabelLocator;
    }

    public By getColorBlindModeLocator() {
        return colorBlindModeLocator;
    }

    public By getSelectedLabelTickLocator() {
        return selectedLabelTickLocator;
    }

    public By getEditLabelLocator() {
        return editLabelLocator;
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getWindow() {
        return getElement(getWindowLocator());
    }

    public PageElement getLabelInput() {
        return getElement(getLabelInputLocator());
    }

    public PageElement getCreateNewLabelButton() {
        return getElement(getCreateNewLabelLocator());
    }

    public PageElement getColorBlindModeLink() {
        return getElement(getColorBlindModeLocator());
    }

    public PageElement getSelectedLabelTick() {
        return getElement(getSelectedLabelTickLocator());
    }

    public PageElement getEditLabelButton() {
        return getElement(getEditLabelLocator());
    }
}
