package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Delete;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class DeleteBoardPopupHTML extends BasePage{

    private final By closePopupLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By permanentlyDeleteLocator = By.xpath("//input[@class='js-confirm full negate' and @type='submit']");
    private WebDriver driver;

    public DeleteBoardPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getClosePopupLocator() {
        return closePopupLocator;
    }

    public By getPermanentlyDeleteLocator() {
        return permanentlyDeleteLocator;
    }

    public PageElement getClosePopupButton() {
        return getElement(getClosePopupLocator());
    }

    public PageElement getPermanentlyDeleteButton() {
        return getElement(getPermanentlyDeleteLocator());
    }
}
