package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteComment;

import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Interfaces.Deletable;
import org.openqa.selenium.WebDriver;

public class DeleteCommentPopup implements Closeable, Deletable{


    private WebDriver driver;
    private DeleteCommentPopupHTML html;

    public DeleteCommentPopup(WebDriver driver) {
        this.driver = driver;
        this.html = new DeleteCommentPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    @Override
    public void delete() {
        html.getDeleteButton().click();
    }
}
