package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.More;


import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic.GenericMenu;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Close.CloseBoardPopup;
import TrelloTesting.Objects.Pages.DeleteWorkPagePage.DeletedWorkPagePage;
import org.openqa.selenium.WebDriver;

public class MoreMenu extends GenericMenu {

    private WebDriver driver;
    private MoreMenuHTML html;

    public MoreMenu(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new MoreMenuHTML(driver);
    }

    private CloseBoardPopup closeBoard(){
        html.getCloseBoardButton().click();
        return new CloseBoardPopup(driver);
    }

    public DeletedWorkPagePage deleteBoard(){
        return closeBoard().deleteBoard();
    }
}
