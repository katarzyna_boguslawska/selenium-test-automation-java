package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteCard;


import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.ShareAndMore.ShareAndMorePopup;
import org.openqa.selenium.WebDriver;

public class DeleteCardPopup implements Closeable {


    private WebDriver driver;
    private DeleteCardPopupHTML html;

    public DeleteCardPopup(WebDriver driver) {
        this.driver = driver;
        this.html = new DeleteCardPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    public ShareAndMorePopup goBackToPopup(ShareAndMorePopup popup){
        html.getBackArrowButton().click();
        return popup;
    }

    public void deleteCard(){
        html.getDeleteCardButton().click();
    }

}
