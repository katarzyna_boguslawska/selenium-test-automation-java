package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteCard;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class DeleteCardPopupHTML extends BasePage {

    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By deleteCardLocator = By.xpath("//input[@class='js-confirm full negate' and @type='submit']");
    private final By backArrowLocator = By.xpath("//a[@class='pop-over-header-back-btn icon-sm icon-back is-shown']");
    private WebDriver driver;

    public DeleteCardPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getDeleteCardLocator() {
        return deleteCardLocator;
    }

    public By getBackArrowLocator() {
        return backArrowLocator;
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getDeleteCardButton() {
        return getElement(getDeleteCardLocator());
    }

    public PageElement getBackArrowButton() {
        return getElement(getBackArrowLocator());
    }
}
