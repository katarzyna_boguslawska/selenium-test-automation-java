package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist.Components;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.WebDriver;


public class ChecklistItem extends BasePage {

    private WebDriver driver;
    private PageElement parent;
    public String text;
    private ChecklistItemHTML html;

    public ChecklistItem(WebDriver driver, PageElement parent, String text){
        super(driver);
        this.driver = driver;
        this.parent = parent;
        this.text = text;
        this.html = new ChecklistItemHTML(driver);
    }

    public void markAsDone(){
        getElement(parent, html.getDoneCheckboxLocator()).click();
    }
}
