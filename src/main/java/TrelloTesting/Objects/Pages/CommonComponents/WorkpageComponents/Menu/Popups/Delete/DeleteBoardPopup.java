package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Delete;


import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Pages.DeleteWorkPagePage.DeletedWorkPagePage;
import org.openqa.selenium.WebDriver;

public class DeleteBoardPopup extends BasePage implements Closeable {

    private WebDriver driver;
    private DeleteBoardPopupHTML html;

    public DeleteBoardPopup(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new DeleteBoardPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getClosePopupButton().click();
    }

    public DeletedWorkPagePage permanentlyDelete(){
        html.getPermanentlyDeleteButton().click();
        return new DeletedWorkPagePage(driver);
    }
}
