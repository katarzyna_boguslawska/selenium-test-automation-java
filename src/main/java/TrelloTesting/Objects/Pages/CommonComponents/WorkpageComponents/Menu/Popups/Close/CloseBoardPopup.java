package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Close;


import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Pages.DeleteBoardConfirmationPage.DeleteBoardConfirmationPage;
import TrelloTesting.Objects.Pages.DeleteWorkPagePage.DeletedWorkPagePage;
import org.openqa.selenium.WebDriver;

public class CloseBoardPopup extends BasePage implements Closeable{

    private WebDriver driver;
    private CloseBoardPopupHTML html;

    public CloseBoardPopup(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new CloseBoardPopupHTML(driver);
    }

    private DeleteBoardConfirmationPage closeBoard(){
        html.getCloseBoardButton().click();
        return new DeleteBoardConfirmationPage(driver);
    }

    public DeletedWorkPagePage deleteBoard(){
        return closeBoard().confirmClosing();
    }

    @Override
    public void close() {
        html.getClosePopupButton().click();
    }
}
