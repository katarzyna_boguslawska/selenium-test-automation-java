package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Member;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class MembersPopupHTML extends BasePage{

    private final By windowLocator = By.xpath("//div[@class='pop-over is-shown']");
    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By memberInputLocator = By.xpath("//input[@class='js-search-mem js-autofocus']");
    private WebDriver driver;

    public MembersPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getWindowLocator() {
        return windowLocator;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getMemberInputLocator() {
        return memberInputLocator;
    }

    public By getMemberOptionLocator(String member){
        return By.xpath("//a[@class='name js-select-member ' and starts-with(@title, '" + member + "')]");
    }

    public PageElement getWindow() {
        return getElement(getWindowLocator());
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getMemberInput() {
        return getElement(getMemberInputLocator());
    }

    public PageElement getMemberOption(String member){
        return getElement(getMemberOptionLocator(member));
    }
}
