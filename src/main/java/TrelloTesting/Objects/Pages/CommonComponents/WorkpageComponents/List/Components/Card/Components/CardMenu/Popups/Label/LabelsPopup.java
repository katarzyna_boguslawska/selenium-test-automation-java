package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Label;


import TrelloTesting.FixedChoices.Colors;
import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.WebDriver;

public class LabelsPopup extends BasePage implements Closeable {

    private WebDriver driver;
    private LabelsPopupHTML html;

    public LabelsPopup(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new LabelsPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    public void searchAndSelect(String searchWhat){
        html.getLabelInput().type(searchWhat).clickEnter();
        close();
    }

    public void label(Colors color){
        getElement(color.getLabelLocator()).click();
        close();
    }

    public void delabel(Colors color){
        PageElement label = getElement(color.getLabelLocator());
        getElement(label, html.getSelectedLabelTickLocator()).click();
    }
}
