package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Attachment;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class AttachmentPopupHTML extends BasePage{

    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By windowLocator = By.xpath("//div[@class='pop-over is-shown']");
    private final By linkInputLocator = By.xpath("//input[@id='addLink']");
    private final By addLinkLocator = By.xpath("//input[@class='js-add-attachment-url']");
    private WebDriver driver;

    public AttachmentPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getWindowLocator() {
        return windowLocator;
    }

    public By getLinkInputLocator() {
        return linkInputLocator;
    }

    public By getAddLinkLocator() {
        return addLinkLocator;
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getWindow() {
        return getElement(getWindowLocator());
    }

    public PageElement getLinkInput() {
        return getElement(getLinkInputLocator());
    }

    public PageElement getAddLinkButton() {
        return getElement(getAddLinkLocator());
    }
}
