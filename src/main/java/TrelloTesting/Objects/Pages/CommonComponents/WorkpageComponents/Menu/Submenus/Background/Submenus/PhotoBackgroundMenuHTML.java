package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Submenus;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class PhotoBackgroundMenuHTML extends BasePage{

    private final By genericPhotoLocator = By.xpath("//div[@class='board-background-select']");
    private WebDriver driver;

    public PhotoBackgroundMenuHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getGenericPhotoLocator() {
        return genericPhotoLocator;
    }

    public PageElement getGenericPhoto(){
        return getElement(getGenericPhotoLocator());
    }
}
