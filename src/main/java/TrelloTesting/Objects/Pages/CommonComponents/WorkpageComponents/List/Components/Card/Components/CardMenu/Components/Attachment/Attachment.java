package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Attachment;

import TrelloTesting.Objects.Base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Attachment extends BasePage {

    private WebDriver driver;
    public By parent;
    private final String link;

    public Attachment(WebDriver driver, By parent, String link){
        super(driver);
        this.driver = driver;
        this.parent = parent;
        this.link = link;
    }
}
