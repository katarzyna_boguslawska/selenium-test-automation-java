package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Emoji;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class EmojiPopupHTML extends BasePage {

    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By windowLocator = By.xpath("//div[@class='pop-over is-shown']");
    private final By searchEmojiLocator = By.xpath("//input[@class='js-filter-emoji js-autofocus']");
    private WebDriver driver;

    public EmojiPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getWindowLocator() {
        return windowLocator;
    }

    public By getSearchEmojiLocator() {
        return searchEmojiLocator;
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getWindow() {
        return getElement(getWindowLocator());
    }

    public PageElement getSearchEmojiInput() {
        return getElement(getSearchEmojiLocator());
    }
}
