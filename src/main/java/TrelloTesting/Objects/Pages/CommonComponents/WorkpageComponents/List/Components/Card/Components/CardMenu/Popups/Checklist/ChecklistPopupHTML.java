package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Checklist;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class ChecklistPopupHTML extends BasePage{

    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By windowLocator = By.xpath("//div[@class='pop-over is-shown']");
    private final By checklistTitleLocator = By.xpath("//input[@id='id-checklist']");
    private final By addChecklistLocator = By.xpath("//input[@class='primary wide confirm js-add-checklist']");
    private WebDriver driver;

    public ChecklistPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getWindowLocator() {
        return windowLocator;
    }

    public By getChecklistTitleLocator() {
        return checklistTitleLocator;
    }

    public By getAddChecklistLocator() {
        return addChecklistLocator;
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getWindow() {
        return getElement(getWindowLocator());
    }

    public PageElement getChecklistTitleInput() {
        return getElement(getChecklistTitleLocator());
    }

    public PageElement getAddChecklistButton() {
        return getElement(getAddChecklistLocator());
    }
}
