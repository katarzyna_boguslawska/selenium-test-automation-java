package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DueDate;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class DueDatePopupHTML extends BasePage {

    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By windowLocator = By.xpath("//div[@class='pop-over is-shown']");
    private final By dateInputLocator = By.xpath("//input[@class='datepicker-select-input js-dpicker-date-input js-autofocus']");
    private final By addDueDateLocator = By.xpath("//input[@class='primary wide confirm' and @type='submit']");
    private final By removeDueDateLocator = By.xpath("//button[@class='negate remove-date js-remove-date']");
    private WebDriver driver;

    public DueDatePopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getWindowLocator() {
        return windowLocator;
    }

    public By getDateInputLocator() {
        return dateInputLocator;
    }

    public By getAddDueDateLocator() {
        return addDueDateLocator;
    }

    public By getRemoveDueDateLocator() {
        return removeDueDateLocator;
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getWindow() {
        return getElement(getWindowLocator());
    }

    public PageElement getDateInput() {
        return getElement(getDateInputLocator());
    }

    public PageElement getAddDueDateButton() {
        return getElement(getAddDueDateLocator());
    }

    public PageElement getRemoveDueDateButton() {
        return getElement(getRemoveDueDateLocator());
    }
}
