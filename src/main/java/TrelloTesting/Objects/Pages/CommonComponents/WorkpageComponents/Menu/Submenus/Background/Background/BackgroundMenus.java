package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Background;


import TrelloTesting.FixedChoices.Colors;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic.GenericMenu;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Submenus.PhotoBackgroundMenuHTML;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.ThreadLocalRandom;

public class BackgroundMenus extends GenericMenu {
    private WebDriver driver;
    private BackgroundMenuHTML html;



    public BackgroundMenus(WebDriver driver){
        super(driver);
        this.driver = driver;
        this.html = new BackgroundMenuHTML(driver);
    }

    public ColorBackgroundMenu chooseColorMenu(){
        html.getColorBackground().click();
        return new ColorBackgroundMenu(driver);
    }

    public PhotoBackgroundMenu choosePhotoMenu(){
        html.getPhotoBackground().click();
        return new PhotoBackgroundMenu(driver);
    }

    public void changeBackgroundToColor(Colors color){
        ColorBackgroundMenu colorMenu = chooseColorMenu();
        colorMenu.chooseColor(color);
    }

    public void changeBackgroundToPhoto(){
        PhotoBackgroundMenu photoMenu = choosePhotoMenu();
        photoMenu.chooseRandomPhoto();
    }
}

class ColorBackgroundMenu extends GenericMenu{

    public ColorBackgroundMenu(WebDriver driver) {
        super(driver);
    }

    public void chooseColor(Colors color){
        switch(color){
            case BLUE:
                getElement(Colors.BLUE.getBackgroundLocator()).click();
                break;
            case ORANGE:
                getElement(Colors.ORANGE.getBackgroundLocator()).click();
                break;
            case GREEN:
                getElement(Colors.GREEN.getBackgroundLocator()).click();
                break;
            case RED:
                getElement(Colors.RED.getBackgroundLocator()).click();
                break;
            case PURPLE:
                getElement(Colors.PURPLE.getBackgroundLocator()).click();
                break;
            case PINK:
                getElement(Colors.PINK.getBackgroundLocator()).click();
                break;
            case PISTACHIO:
                getElement(Colors.PISTACHIO.getBackgroundLocator()).click();
                break;
            case TURQUOISE:
                getElement(Colors.TURQUOISE.getBackgroundLocator()).click();
                break;
            case GRAY:
                getElement(Colors.GRAY.getBackgroundLocator()).click();
        }
    }
}

class PhotoBackgroundMenu extends GenericMenu{
    private PhotoBackgroundMenuHTML html;

    public PhotoBackgroundMenu(WebDriver driver) {
        super(driver);
        this.html = new PhotoBackgroundMenuHTML(driver);
    }

    private int getRandomIndex(){
        return ThreadLocalRandom.current().nextInt(0, 12);
    }

    public void chooseRandomPhoto(){
        int index = getRandomIndex();
        getElements(html.getGenericPhotoLocator()).get(index).click();
    }
}
