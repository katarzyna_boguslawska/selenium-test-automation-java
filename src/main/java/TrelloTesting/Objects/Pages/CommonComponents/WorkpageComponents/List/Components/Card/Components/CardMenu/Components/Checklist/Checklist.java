package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist.Components.ChecklistItem;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.*;

public class Checklist extends BasePage {

    private WebDriver driver;
    public String title;
    private By parent;
    public java.util.List<ChecklistItem> items;
    private ChecklistHTML html;

    public Checklist(WebDriver driver, String title, By parent){
        super(driver);
        this.driver = driver;
        this.title = title;
        this.parent = parent;
        this.items = new ArrayList<ChecklistItem>();
        this.html = new ChecklistHTML(driver);
    }

    public int countItems(){
        return getElements(parent, html.getGenericItemLocator()).size();
    }

    private PageElement getItemConatiner(String content){
        return getElement(parent, By.xpath(".//div[@class='checklist-item' and .//p[text()='" + content + "']]"));
    }

    private ChecklistItem makeItem(String content){
        getElement(parent, html.getItemContentLocator()).focusAndType(content);
        getElement(parent, html.getAddItemLocator()).click();
        getElement(parent, html.getQuitAddingItemsLocator()).click();
        PageElement container = getItemConatiner(content);
        return new ChecklistItem(driver, container, content);
    }

    public void addItem(String content){
        ChecklistItem item = makeItem(content);
        items.add(item);

    }

    public ChecklistItem getItemByContent(String content){
        for(ChecklistItem item : items){
            if (item.text.equals(content)){
                return item;
            }
        }
        return null;
    }

    public void removeChecklist(){
        getElement(parent, html.getDeleteChecklistLocator()).click();
        html.getDeleteButton().click();
        html.getCloseButton().click();
    }

    public Boolean hasTitle(String title){
        String actualTitle = getElement(parent, html.getGenericHeaderLocator()).getText();
        return actualTitle.equals(title) ? Boolean.TRUE : Boolean.FALSE;
    }

    public Boolean isProgressAsExpected(double numberOfCompletedTasks){
        slowDown(500);
        double completionPercentage = (numberOfCompletedTasks / items.size()) * 100.0f;
        int rounded = (int)Math.round(completionPercentage);
        String displayedProgress = getElement(parent, html.getProgressBarLocator()).getText().substring(0,2);
        int displayedNumericProgress = Integer.parseInt(displayedProgress);
        return rounded == displayedNumericProgress;
    }

    public void completeTask(String whichTask){
        getItemByContent(whichTask).markAsDone();
    }
}