package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class ListHTML extends BasePage{

    private final By archiveListLocator = By.xpath(".//a[@class='js-close-list']");
    private final By listParentNodesLocator = By.xpath("//div[@class='js-list list-wrapper']");

    private final By moreMenuLocator = By.xpath("./div/div/div[@class='list-header-extras']/a");
    private final By addCardLocator = By.xpath("./div/a[@class='open-card-composer js-open-card-composer']");
    private final By cardTextLocator = By.xpath(".//textarea[@class='list-card-composer-textarea js-card-title']");
    private final By addLocator = By.xpath(".//input[@class='primary confirm mod-compact js-add-card']");
    private final By quitAddingLocator = By.xpath(".//a[@class='icon-lg icon-close dark-hover js-cancel']");
    private WebDriver driver;

    public ListHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getArchiveListLocator() {
        return archiveListLocator;
    }

    public By getListParentNodesLocator() {
        return listParentNodesLocator;
    }

    public By getMoreMenuLocator() {
        return moreMenuLocator;
    }

    public By getAddCardLocator() {
        return addCardLocator;
    }

    public By getCardTextLocator() {
        return cardTextLocator;
    }

    public By getAddLocator() {
        return addLocator;
    }

    public By getQuitAddingLocator() {
        return quitAddingLocator;
    }

    public By getCardParentLocator(String textOnCard){
        return By.xpath("//a[.='" + textOnCard + "']/../..");
    }

    public By getListHeaderLocator(String title){
        return By.xpath(".//h2[text()='" + title + "']");
    }

    public PageElement getArchiveListButton() {
        return getElement(getArchiveListLocator());
    }

    public PageElement getListParentNodesButton() {
        return getElement(getListParentNodesLocator());
    }

    public PageElement getMoreMenuButton() {
        return getElement(getMoreMenuLocator());
    }

    public PageElement getAddCardButton() {
        return getElement(getAddCardLocator());
    }

    public PageElement getCardTextButton() {
        return getElement(getCardTextLocator());
    }

    public PageElement getAddButton() {
        return getElement(getAddLocator());
    }

    public PageElement getQuitAddingButton() {
        return getElement(getQuitAddingLocator());
    }
}
