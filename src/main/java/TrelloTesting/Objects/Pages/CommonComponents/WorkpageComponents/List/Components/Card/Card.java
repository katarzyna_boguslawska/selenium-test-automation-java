package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card;

import TrelloTesting.FixedChoices.Colors;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.CardMenu;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Card extends BasePage {

    public CardMenu menu;

    private WebDriver driver;
    public By parent;
    public String text;
    public By stableParent;
    private CardHTML html;

    public Card(WebDriver driver, By parent, String textOnCard) {
        super(driver);
        this.driver = driver;
        this.parent = parent;
        this.text = textOnCard;
        this.stableParent = By.xpath("//a[contains(@class, 'list-card js-member-droppable ui-droppable') and .//*[text()='" + textOnCard + "']]");
        this.html = new CardHTML(driver);
    }

    public CardMenu showCardMenu(){
        if (menu == null){
            getElement(stableParent).click();
            menu =  new CardMenu(driver, this);
        } else {
            getElement(stableParent).click();
        }
        return menu;
    }

    public CardMenu showCardMenu(Boolean noClick){
        return menu;
    }

    public void dragAndDropCard(By destinationElement){
        getElement(stableParent).dragTo(destinationElement);
    }

    public Boolean dueDateWasAdded(){
        PageElement dateIndicator = getElement(stableParent, html.getFutureDueDateLocator());
        return dateIndicator != null ? Boolean.TRUE : Boolean.FALSE;
    }

    public Boolean attachmentWasAdded(){
        return isVisible(By.xpath("//a[contains(@class, 'list-card js-member-droppable') and .//*[text()='" + text + "']]//div[@class='list-card-cover js-card-cover']"));
    }

    public Boolean hasLabelOfColor(Colors color){
        if (color.equals(Colors.GREEN)){
            return getElement(stableParent, Colors.GREEN.getCheckLabelLocator()) != null ? Boolean.TRUE : Boolean.FALSE;
        }
        else if (color.equals(Colors.YELLOW)){
            return getElement(stableParent, Colors.YELLOW.getCheckLabelLocator()) != null ? Boolean.TRUE : Boolean.FALSE;
        }
        else if (color.equals(Colors.ORANGE)){
            return getElement(stableParent, Colors.ORANGE.getCheckLabelLocator()) != null ? Boolean.TRUE : Boolean.FALSE;
        }
        else if (color.equals(Colors.RED)){
            return getElement(stableParent, Colors.RED.getCheckLabelLocator()) != null ? Boolean.TRUE : Boolean.FALSE;
        }
        else if (color.equals(Colors.PURPLE)){
            return getElement(stableParent, Colors.PURPLE.getCheckLabelLocator()) != null ? Boolean.TRUE : Boolean.FALSE;
        }
        else {
            return getElement(stableParent, Colors.BLUE.getCheckLabelLocator()) != null ? Boolean.TRUE : Boolean.FALSE;
        }
    }

    public Boolean MemberWasAdded(String who){
        By memberLocator = html.getMemberLocator(who);
        return getElement(stableParent, memberLocator) != null ? Boolean.TRUE : Boolean.FALSE;
    }

    public Boolean hasNoComment(){
        showCardMenu().hideMenu();
        return isAbsent(stableParent, html.getCommentBadgeLocator());
    }
}