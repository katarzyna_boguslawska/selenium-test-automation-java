package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Checklist;


import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist.Checklist;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ChecklistPopup implements Closeable {


    private WebDriver driver;
    private ChecklistPopupHTML html;

    public ChecklistPopup(WebDriver driver) {
        this.driver = driver;
        this.html = new ChecklistPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    private void completeTitle(String title){
        html.getChecklistTitleInput().type(title);
    }

    public Checklist addChecklist(String title, By parent){
        completeTitle(title);
        html.getAddChecklistButton().click();
        return new Checklist(driver, title, parent);
    }
}
