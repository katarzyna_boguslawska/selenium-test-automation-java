package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Comment;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class CommentHTML extends BasePage{
    private final By editLocator = By.xpath(".//a[@class='js-edit-action']");
    private final By deleteLocator = By.xpath(".//a[@class='js-confirm-delete-action']");
    private final By  commentContentLocator = By.xpath(".//div[@class='current-comment js-friendly-links js-open-card']/p");
    private WebDriver driver;

    public CommentHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getEditLocator() {
        return editLocator;
    }

    public By getDeleteLocator() {
        return deleteLocator;
    }

    public By getCommentContentLocator() {
        return commentContentLocator;
    }

    public By getActivitiesCommentLocator(String commentContent) {
        return By.xpath("//div[@class='phenom mod-comment-type' and .//p[text()='" + commentContent + "'] and .//a[@class='js-confirm-delete-action']]");
    }

    public PageElement getEditButton() {
        return getElement(getEditLocator());
    }

    public PageElement getDeleteLink() {
        return getElement(getDeleteLocator());
    }

    public PageElement getCommentContentArea() {
        return getElement(getCommentContentLocator());
    }

    public PageElement getActivitiesCommentArea(String commentContent) {
        return getElement(getActivitiesCommentLocator(commentContent));
    }
}
