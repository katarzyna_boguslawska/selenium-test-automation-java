package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Member;


import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Base.BasePage;
import org.openqa.selenium.WebDriver;

public class MembersPopup extends BasePage implements Closeable{

    private WebDriver driver;
    private MembersPopupHTML html;

    public MembersPopup(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new MembersPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    public void searchAndSelect(String member){
        html.getMemberInput().type(member).clickEnter();
        close();
    }

    private void chooseMemberFromList(String member){
        html.getMemberOption(member).click();
    }

    public void addMember(String member){
        chooseMemberFromList(member);
        close();
    }
}
