package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu;

import TrelloTesting.FixedChoices.Colors;
import TrelloTesting.FixedChoices.Emojis;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Attachment.Attachment;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Card;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist.Checklist;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Comment.Comment;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Attachment.AttachmentPopup;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Checklist.ChecklistPopup;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DueDate.DueDatePopup;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Emoji.EmojiPopup;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Label.LabelsPopup;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Member.MembersPopup;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.ShareAndMore.ShareAndMorePopup;
import TrelloTesting.Objects.Pages.JsonExportedPage.JsonExportedPage;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class CardMenu extends BasePage {

    private WebDriver driver;
    private Card card;
    private List<Checklist> checklists;
    private List<Attachment> attachments;
    private List<Comment> comments;
    private CardMenuHTML html;

    public CardMenu(WebDriver driver, Card card){
        super(driver);
        this.driver = driver;
        this.card = card;
        this.checklists = new ArrayList<Checklist>();
        this.comments = new ArrayList<Comment>();
        this.attachments = new ArrayList<Attachment>();
        this.html = new CardMenuHTML(driver);
    }

    public void hideMenu(){
        try{
            html.getClose().click();
        } catch (TimeoutException e){
            html.getCloseOnCover().click();
        }
    }

    private MembersPopup openMemberPopup(){
        html.getMembersButton().click();
        return new MembersPopup(driver);
    }

    private LabelsPopup openLabelPopup(){
        html.getLabelsButton().click();
        return new LabelsPopup(driver);
    }

    private AttachmentPopup openAttachmentPopup(){
        html.getAttachmentButton().click();
        return new AttachmentPopup(driver);
    }

    private ChecklistPopup openChecklistPopup(){
        html.getChecklistButton().click();
        return new ChecklistPopup(driver);
    }

    private DueDatePopup openDueDatePopup(){
        html.getDueDateButton().click();
        return new DueDatePopup(driver);
    }

    private ShareAndMorePopup openShareAndMorePopup(){
        html.getShareAndMoreLink().click();
        return new ShareAndMorePopup(driver);
    }

    private EmojiPopup openEmojiPopup(){
        getElement(html.getWindowLocator(), html.getEmojiLocator()).click();
        return new EmojiPopup(driver);
    }
    public void addDueDate(String date){
        DueDatePopup popup = openDueDatePopup();
        popup.addDueDate(date);
        hideMenu();
    }
    public void archiveCard(){
        html.getArchiveButton().click();
        hideMenu();
    }

    private By getAttachmentContainer(String link){
        String filename = link.substring(link.lastIndexOf("/") + 1);
        return html.getAttachmentContainerLocator(filename);
    }

    private Attachment makeAttachmentFromLink(String link){
        By parent = getAttachmentContainer(link);
        AttachmentPopup popup = openAttachmentPopup();
        return popup.addAttachment(link, parent);
    }

    public void addAttachmentFromLink(String link){
        Attachment attachment = makeAttachmentFromLink(link);
        attachments.add(attachment);
        hideMenu();
    }

    public JsonExportedPage exportToJson(){
        ShareAndMorePopup popup = openShareAndMorePopup();
        return popup.exportToJson(this.card);
    }

    private By getChecklistContainer(String title){
        return html.getChecklistContainerLocator(title);
    }

    private Checklist makeChecklist(String title){
        By container = getChecklistContainer(title);
        ChecklistPopup popup = openChecklistPopup();
        return popup.addChecklist(title, container);
    }

    public void addChecklist(String title){
        Checklist checklist = makeChecklist(title);
        checklists.add(checklist);
    }

    public void removeChecklist(String checklistTitle){
        Checklist checklist = findChecklistByTitle(checklistTitle);
        checklist.removeChecklist();
    }

    public void populateChecklist(String checklistTitle, String taskName){
        Checklist checklist = findChecklistByTitle(checklistTitle);
        for (int i = 0; i < 4; i++){
            String text = taskName + Integer.toString(i + 1);
            checklist.addItem(text);
        }
    }

    public int countChecklists(){
        return getElements(html.getGenericChecklistLocator()).size();
    }

    public Checklist findChecklistByTitle(String title){
        for(Checklist checklist : checklists){
            if (checklist.title.equals(title)){
                return checklist;
            }
        }
        return null;
    }

    public void label(Colors color){
        LabelsPopup popup = openLabelPopup();
        popup.label(color);
        hideMenu();
    }
    public void addMember(String who){
        MembersPopup popup = openMemberPopup();
        popup.addMember(who);
        hideMenu();
    }

    private PageElement getCommentContainer(String commentText){
        return getElement(html.getWindowLocator(), html.getCommentContainerLocator(commentText));
    }

    private Comment makeComment(String commentText){
        getElement(html.getWindowLocator(), html.getCommentLocator()).focusAndType(commentText);
        getElement(html.getWindowLocator(), html.getSubmitCommentLocator()).click();
        PageElement container = getCommentContainer(commentText);
        return new Comment(driver, commentText, container);
    }

    private Comment makeComment(String commentText, Emojis emoji){
        getElement(html.getWindowLocator(), html.getCommentLocator()).focusAndType(commentText);
        EmojiPopup popup = openEmojiPopup();
        popup.insertEmoji(emoji);
        getElement(html.getWindowLocator(), html.getSubmitCommentLocator()).click();
        PageElement container = getCommentContainer(commentText);
        return new Comment(driver, commentText, container);
    }

    public void comment(String commentText){
        Comment comment = makeComment(commentText);
        comments.add(comment);
    }

    public void comment(String commentText, Emojis emoji){
        Comment comment = makeComment(commentText, emoji);
        comments.add(comment);
    }

    public Comment findCommentByText(String commentText){
        for (Comment comment : comments){
            if (comment.text.equals(commentText)){
                return comment;
            }
        }
        return null;
    }

    public Comment findCommentByText(String commentText, Boolean tolerance){
        for (Comment comment : comments){
            if (comment.text.startsWith(commentText)){
                return comment;
            }
        }
        return null;
    }

    public void deleteCommentWithContent(String commentText){
        Comment toDelete = findCommentByText(commentText);
        toDelete.delete();
        comments.remove(toDelete);
        hideMenu();
    }

    public Boolean isCommentRemoved(String commentText){
        Boolean isRemoved = isAbsent(getElement(html.getWindowLocator()), html.getCommentTextareaLocator(commentText));
        hideMenu();
        return isRemoved;
    }

    public Boolean wasChecklistAdded(String title){
        return findChecklistByTitle(title) != null ? Boolean.TRUE : Boolean.FALSE;
    }

    public Boolean wasChecklistWithTitlePopulated(String checklistTitle, int tasksNumber){
        Checklist checklist = findChecklistByTitle(checklistTitle);
        return checklist.countItems() == tasksNumber ? Boolean.TRUE : Boolean.FALSE;
    }

    public Boolean wereTasksCompleted(String checklistTitle, double howManyTasks){
        Checklist checklist = findChecklistByTitle(checklistTitle);
        return checklist.isProgressAsExpected(howManyTasks);
    }

    public Boolean wasCommentAdded(String commentText){
        By commentLocator = html.getCommentTextareaLocator(commentText);
        try{
            PageElement comment = getElement(commentLocator);
            hideMenu();
            return Boolean.TRUE;
        } catch (TimeoutException e){
            hideMenu();
            return Boolean.FALSE;
        }
    }
}
