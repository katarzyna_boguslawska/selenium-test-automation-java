package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist.Components;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class ChecklistItemHTML extends BasePage{

    private final By doneCheckbox = By.xpath(".//span[@class='icon-sm icon-check checklist-item-checkbox-check']");
    private WebDriver driver;

    public ChecklistItemHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getDoneCheckboxLocator() {
        return doneCheckbox;
    }

    public PageElement getDoneCheckbox() {
        return getElement(getDoneCheckboxLocator());
    }
}
