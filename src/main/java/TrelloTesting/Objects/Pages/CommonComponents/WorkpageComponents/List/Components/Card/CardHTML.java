package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class CardHTML extends BasePage{

    private final By cardMenuLocator = By.xpath(".//a[@class='list-card-title js-card-name']");
    private final By futureDueDateLocator = By.xpath(".//div[@class='badge is-due-future']");
    private final By imageLocator = By.xpath(".//div[@class='list-card-cover js-card-cover']");
    private final By commentBadgeLocator = By.xpath(".//div[@class='badge' and @title='Comments']");
    private WebDriver driver;

    public CardHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCardMenuLocator() {
        return cardMenuLocator;
    }

    public By getFutureDueDateLocator() {
        return futureDueDateLocator;
    }

    public By getImageLocator() {
        return imageLocator;
    }

    public By getCommentBadgeLocator() {
        return commentBadgeLocator;
    }

    public By getMemberLocator(String who){ return By.xpath(".//span[@class='member-initials' and starts-with(@title,  '" + who + "')]"); }

    public PageElement getCardMenuLink() {
        return getElement(getCardMenuLocator());
    }

    public PageElement getFutureDueDateBadge() {
        return getElement(getFutureDueDateLocator());
    }

    public PageElement getImageBadge() {
        return getElement(getImageLocator());
    }

    public PageElement getCommentBadge() {
        return getElement(getCommentBadgeLocator());
    }
}
