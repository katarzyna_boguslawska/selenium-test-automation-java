package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Comment;

import TrelloTesting.FixedChoices.Emojis;
import TrelloTesting.Interfaces.Deletable;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteComment.DeleteCommentPopup;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;


public class Comment extends BasePage implements Deletable{

    private WebDriver driver;
    public String text;
    public PageElement parent;
    private CommentHTML html;

    public Comment(WebDriver driver, String text, PageElement parent){
        super(driver);
        this.driver = driver;
        this.text = text;
        this.parent = parent;
        this.html = new CommentHTML(driver);
    }

    public Boolean checkIfHasEmoji(Emojis emoji){
        try {
            getElement(emoji.getEmojiImage());
            return Boolean.TRUE;
        } catch (TimeoutException e){
            return Boolean.FALSE;
        }
    }

    public String getCommentText(){
        return getElement(parent, html.getCommentContentLocator()).getText();
    }

    private DeleteCommentPopup openDeletePopup(){
        getElement(html.getActivitiesCommentLocator(text), html.getDeleteLocator()).click();
//        html.getDeleteLink().click();
        return new DeleteCommentPopup(driver);
    }

    public void delete(){
        DeleteCommentPopup popup = openDeletePopup();
        popup.delete();
    }
}
