package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public final class GenericMenuHTML extends BasePage{
    private final By closeMenuLocator = By.xpath("//a[@class='board-menu-header-close-button icon-lg icon-close js-hide-sidebar']");
    private final By backArrowMenuButton = By.xpath("//a[@class='board-menu-header-back-button icon-lg icon-back js-pop-widget-view']");
    private final By moreLocator = By.xpath("//a[@class='board-menu-navigation-item-link js-open-more']");
    private WebDriver driver;

    public GenericMenuHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseMenuLocator() {
        return closeMenuLocator;
    }

    public By getBackArrowMenuLocator() {
        return backArrowMenuButton;
    }

    public By getMoreLocator() {
        return moreLocator;
    }

    public PageElement getCloseMenuButton() {
        return getElement(getCloseMenuLocator());
    }

    public PageElement getBackArrowMenuButton() {
        return getElement(getBackArrowMenuLocator());
    }

    public PageElement getMoreButton() {
        return getElement(getMoreLocator());
    }
}
