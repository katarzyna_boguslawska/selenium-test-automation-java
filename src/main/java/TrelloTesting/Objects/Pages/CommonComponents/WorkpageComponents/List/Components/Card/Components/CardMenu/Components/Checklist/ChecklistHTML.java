package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class ChecklistHTML extends BasePage{

    private final By deleteChecklistLocator = By.xpath(".//a[@class='hide-on-edit quiet js-confirm-delete']");
    private final By closeLocator = By.xpath("//a[@class='icon-lg icon-close dialog-close-button js-close-window']");
    private final By deleteLocator = By.xpath("//input[@value='Delete Checklist']");

    private final By genericItemLocator = By.xpath(".//div[@class='checklist-item']");
    private final By progressBarLocator = By.xpath(".//span[@class='checklist-progress-percentage js-checklist-progress-percent']");
    private final By genericHeaderLocator = By.xpath(".//h3");

    private WebDriver driver;

    //item submenu locators
    private final By itemContentLocator = By.xpath(".//textarea[@class='checklist-new-item-text js-new-checklist-item-input']");
    private final By addItemLocator = By.xpath(".//input[@class='primary confirm mod-submit-edit js-add-checklist-item']");
    private final By quitAddingItemsLocator = By.xpath(".//a[@class='icon-lg icon-close dark-hover cancel js-cancel-checklist-item']");

    public ChecklistHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getDeleteChecklistLocator() {
        return deleteChecklistLocator;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getDeleteLocator() {
        return deleteLocator;
    }

    public By getGenericItemLocator() {
        return genericItemLocator;
    }

    public By getProgressBarLocator() {
        return progressBarLocator;
    }

    public By getGenericHeaderLocator() {
        return genericHeaderLocator;
    }

    public By getItemContentLocator() {
        return itemContentLocator;
    }

    public By getAddItemLocator() {
        return addItemLocator;
    }

    public By getQuitAddingItemsLocator() {
        return quitAddingItemsLocator;
    }

    public PageElement getDeleteChecklistButton() {
        return getElement(getDeleteChecklistLocator());
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getDeleteButton() {
        return getElement(getDeleteLocator());
    }

    public PageElement getGenericItem() {
        return getElement(getGenericItemLocator());
    }

    public PageElement getProgressBarField() {
        return getElement(getProgressBarLocator());
    }

    public PageElement getGenericHeader() {
        return getElement(getGenericHeaderLocator());
    }

    public PageElement getItemContentField() {
        return getElement(getItemContentLocator());
    }

    public PageElement getAddItemButton() {
        return getElement(getAddItemLocator());
    }

    public PageElement getQuitAddingItemsButton() {
        return getElement(getQuitAddingItemsLocator());
    }
}
