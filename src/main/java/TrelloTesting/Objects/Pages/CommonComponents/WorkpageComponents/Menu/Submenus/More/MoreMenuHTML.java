package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.More;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public final class MoreMenuHTML extends BasePage {
    private final By closeBoardLocator = By.xpath("//a[@class='board-menu-navigation-item-link js-close-board']");
    private WebDriver driver;

    public MoreMenuHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseBoardLocator() {
        return closeBoardLocator;
    }

    public PageElement getCloseBoardButton(){
        return getElement(getCloseBoardLocator());
    }
}
