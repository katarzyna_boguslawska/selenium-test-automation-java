package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Attachment;

import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Attachment.Attachment;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AttachmentPopup implements Closeable {


    private WebDriver driver;
    private AttachmentPopupHTML html;

    public AttachmentPopup(WebDriver driver) {
        this.driver = driver;
        this.html = new AttachmentPopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    public Attachment addAttachment(String link, By parent){
        html.getLinkInput().type(link);
        html.getAddLinkButton().click();
        return new Attachment(driver, parent, link);
    }
}
