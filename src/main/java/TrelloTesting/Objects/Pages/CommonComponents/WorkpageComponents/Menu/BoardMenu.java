package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu;

import TrelloTesting.FixedChoices.Colors;
import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Background.BackgroundMenus;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic.GenericMenu;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.More.MoreMenu;
import TrelloTesting.FixedChoices.BoardMenuSubmenus;
import TrelloTesting.Objects.Pages.DeleteWorkPagePage.DeletedWorkPagePage;
import org.openqa.selenium.WebDriver;

public class BoardMenu extends GenericMenu implements Closeable{

    private WebDriver driver;
    private BoardMenuHTML html;

    public BoardMenu(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new BoardMenuHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseMenuButton().click();
    }

    private GenericMenu returnMenu(BoardMenuSubmenus submenu){
        switch(submenu){
            case MORE:
                return new MoreMenu(driver);
            case BACKGROUND:
                return new BackgroundMenus(driver);
            default:
                throw new UnsupportedOperationException();
        }
    }

    public GenericMenu showMenu(BoardMenuSubmenus menu){
        isClickable(menu.getSubmenuPositionLocator());
        getElement(menu.getSubmenuPositionLocator()).click();
        return returnMenu(menu);
    }

    public DeletedWorkPagePage deleteBoard(){
        MoreMenu menu = (MoreMenu)showMenu(BoardMenuSubmenus.MORE);
        return menu.deleteBoard();
    }

    public void changeBackgroundToColor(Colors color){
        BackgroundMenus menu = (BackgroundMenus)showMenu(BoardMenuSubmenus.BACKGROUND);
        menu.changeBackgroundToColor(color);
    }

    public void changeBackgroundToRandomPhoto(){
        BackgroundMenus menu = (BackgroundMenus)showMenu(BoardMenuSubmenus.BACKGROUND);
        menu.changeBackgroundToPhoto();

    }
}
