package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.ShareAndMore;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class ShareAndMorePopupHTML extends BasePage{

    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By windowLocator = By.xpath("//div[@class='pop-over is-shown']");
    private final By jsonExportLocator = By.xpath("//a[@class='js-export-json']");
    private final By deleteLocator = By.xpath("//a[@class='js-delete']");
    private WebDriver driver;

    public ShareAndMorePopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getWindowLocator() {
        return windowLocator;
    }

    public By getJsonExportLocator() {
        return jsonExportLocator;
    }

    public By getDeleteLocator() {
        return deleteLocator;
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getWindow() {
        return getElement(getWindowLocator());
    }

    public PageElement getJsonExportOption() {
        return getElement(getJsonExportLocator());
    }

    public PageElement getDeleteLink() {
        return getElement(getDeleteLocator());
    }
}
