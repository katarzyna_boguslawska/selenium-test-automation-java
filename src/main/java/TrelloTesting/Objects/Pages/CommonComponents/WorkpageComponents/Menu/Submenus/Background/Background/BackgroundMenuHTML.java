package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Background;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class BackgroundMenuHTML extends BasePage{

    private final By colorBackground = By.xpath("//div[@class='board-backgrounds-section-tile board-backgrounds-colors-tile js-bg-colors']");
    private final By photoBackground = By.xpath("//div[@class='board-backgrounds-section-tile board-backgrounds-photos-tile js-bg-photos']");
    private WebDriver driver;

    public BackgroundMenuHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getColorBackgroundLocator() {
        return colorBackground;
    }

    public By getPhotoBackgroundLocator() {
        return photoBackground;
    }

    public PageElement getColorBackground() {
        return getElement(getColorBackgroundLocator());
    }

    public PageElement getPhotoBackground() {
        return getElement(getPhotoBackgroundLocator());
    }
}
