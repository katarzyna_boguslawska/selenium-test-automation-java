package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.ShareAndMore;


import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Card;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteCard.DeleteCardPopup;
import TrelloTesting.Objects.Pages.JsonExportedPage.JsonExportedPage;
import org.openqa.selenium.WebDriver;

public class ShareAndMorePopup extends BasePage implements Closeable {

    private WebDriver driver;
    private ShareAndMorePopupHTML html;

    public ShareAndMorePopup(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new ShareAndMorePopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    public JsonExportedPage exportToJson(Card card){
        html.getJsonExportOption().click();
        return new JsonExportedPage(driver, card);
    }

    private DeleteCardPopup openDeleteCardPopup(){
        html.getDeleteLink().click();
        return new DeleteCardPopup(driver);
    }

    public void deleteCard(){
        openDeleteCardPopup().deleteCard();
    }
}
