package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DueDate;


import TrelloTesting.Interfaces.Closeable;
import org.openqa.selenium.WebDriver;

public class DueDatePopup implements Closeable {


    private WebDriver driver;
    private DueDatePopupHTML html;

    public DueDatePopup(WebDriver driver) {
        this.driver = driver;
        this.html = new DueDatePopupHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }

    public void addDueDate(String date){
        html.getDateInput().focusAndType(date).clickEnter();
    }

    public void removeDueDate(){
        html.getRemoveDueDateButton().clickEnter();
    }
}
