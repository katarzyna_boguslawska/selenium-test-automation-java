package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteComment;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class DeleteCommentPopupHTML extends BasePage {

    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By deleteLocator = By.xpath("//input[@class='js-confirm full negate']");
    private WebDriver driver;

    public DeleteCommentPopupHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getDeleteLocator() {
        return deleteLocator;
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getDeleteButton() {
        return getElement(getDeleteLocator());
    }
}
