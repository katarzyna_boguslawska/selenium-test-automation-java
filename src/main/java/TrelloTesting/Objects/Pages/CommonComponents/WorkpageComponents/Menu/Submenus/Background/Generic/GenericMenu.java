package TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic;


import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.BoardMenu;
import org.openqa.selenium.WebDriver;

public class GenericMenu extends BasePage implements Closeable{

    private WebDriver driver;
    private GenericMenuHTML html;

    public GenericMenu(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new GenericMenuHTML(driver);
    }

    @Override
    public void close() {
        html.getCloseMenuButton().click();
    }

    public BoardMenu returnToMainMenu(){
        Boolean isContinue = Boolean.TRUE;
        while (isAbsent(html.getMoreLocator())){
                html.getBackArrowMenuButton().click();
            }
        return new BoardMenu(driver);
    }
}