package TrelloTesting.Objects.Pages.FailedLoginPage;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class FailedLoginPageHTML extends BasePage{

    private final By tFailureLocator = By.xpath("//p[@class='error-message']");
    private final By failureLocator = By.xpath("//div[@id='error']");
    private WebDriver driver;

    public FailedLoginPageHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By gettFailureLocator() {
        return tFailureLocator;
    }

    public By getFailureLocator() {
        return failureLocator;
    }

    public PageElement gettFailure() {
        return getElement(gettFailureLocator());
    }

    public PageElement getFailure() {
        return getElement(getFailureLocator());
    }
}
