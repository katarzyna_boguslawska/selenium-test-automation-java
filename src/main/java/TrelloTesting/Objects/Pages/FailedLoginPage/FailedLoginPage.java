package TrelloTesting.Objects.Pages.FailedLoginPage;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

public class FailedLoginPage extends BasePage {

    private WebDriver driver;
    private FailedLoginPageHTML html;

    public FailedLoginPage(WebDriver driver){
        super(driver);
        this.driver = driver;
        this.html = new FailedLoginPageHTML(driver);
    }

    public Boolean isErrorCommunicated(){
        try{
            PageElement errorMsg = html.getFailure();
            return Boolean.TRUE;
        } catch (TimeoutException e1){
            try {
                PageElement errorMsg = html.gettFailure();
                return Boolean.TRUE;
            } catch (TimeoutException e2){
                return Boolean.FALSE;
            }
        }
    }
}
