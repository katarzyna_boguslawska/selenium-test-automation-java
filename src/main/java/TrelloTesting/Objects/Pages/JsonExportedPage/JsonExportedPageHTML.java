package TrelloTesting.Objects.Pages.JsonExportedPage;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class JsonExportedPageHTML extends BasePage{

    private final By bodyLocator = By.xpath("//body");
    private WebDriver driver;

    public JsonExportedPageHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getBodyLocator() {
        return bodyLocator;
    }

    public PageElement getBody() {
        return getElement(getBodyLocator());
    }
}
