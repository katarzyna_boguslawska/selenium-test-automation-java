package TrelloTesting.Objects.Pages.JsonExportedPage;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Card;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.CardMenu;
import TrelloTesting.Objects.Pages.GroupWorkPage.GroupWorkPage;
import org.openqa.selenium.WebDriver;

public class JsonExportedPage extends BasePage {

    private WebDriver driver;
    private Card card;
    private JsonExportedPageHTML html;

    public JsonExportedPage(WebDriver driver, Card card){
        super(driver);
        this.driver = driver;
        this.card = card;
        this.html = new JsonExportedPageHTML(driver);
    }

    public Boolean isJson(){
        return html.getBody().getText().startsWith("{\"id\":\"");
    }

    public GroupWorkPage unexportFromJson(GroupWorkPage workpage){
        driver.navigate().back();
        CardMenu menu = new CardMenu(driver, card);
        menu.hideMenu();
        return workpage;
    }
}
