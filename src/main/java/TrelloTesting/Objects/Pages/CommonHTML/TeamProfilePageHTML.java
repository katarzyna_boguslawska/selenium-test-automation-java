package TrelloTesting.Objects.Pages.CommonHTML;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class TeamProfilePageHTML extends BasePage {

    private final By membersTabLocator = By.xpath("//a[@class='tabbed-pane-nav-item-button js-org-members' and text()='Members']");
    private final By settingsTabLocator = By.xpath("//a[text()='Settings']");
    private final By boardsTabLocator = By.xpath("//a[text()='Boards']");
    private final By removeMemberLocator = By.xpath("//a[@class='option button-link remove-button']");
    private final By confirmMemberRemovalLocator = By.xpath("//a[@class='js-soft-remove']");
    private WebDriver driver;

    public TeamProfilePageHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getMembersTabLocator() {
        return membersTabLocator;
    }

    public By getSettingsTabLocator() {
        return settingsTabLocator;
    }

    public By getBoardsTabLocator() {
        return boardsTabLocator;
    }

    public By getRemoveMemberLocator() {
        return removeMemberLocator;
    }

    public By getConfirmMemberRemovalLocator() {
        return confirmMemberRemovalLocator;
    }

    public By getTeamProfileHeaderLocator(String teamName){
        return By.xpath("//div[@class='tabbed-pane-header-details-name' and .//h1[text()='" + teamName + "']]");
    }

    public By getNewMemberLocator(String username){
        return By.xpath("//span[starts-with(text(), " + username +")]");
    }

    public PageElement getMembersTabButton() {
        return getElement(getMembersTabLocator());
    }

    public PageElement getSettingsTabButton() {
        return getElement(getSettingsTabLocator());
    }

    public PageElement getBoardsTabButton() {
        return getElement(getBoardsTabLocator());
    }

    public PageElement getRemoveMemberButton() {
        return getElement(getRemoveMemberLocator());
    }

    public PageElement getConfirmMemberRemovalButton() {
        return getElement(getConfirmMemberRemovalLocator());
    }
}
