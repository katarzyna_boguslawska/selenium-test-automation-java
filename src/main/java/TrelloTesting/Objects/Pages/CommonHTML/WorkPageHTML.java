package TrelloTesting.Objects.Pages.CommonHTML;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class WorkPageHTML extends BasePage{

    private final By listTitleInputLocator = By.xpath("//input[@class='list-name-input']");
    private final By saveListLocator = By.xpath("//input[@class='primary mod-list-add-button js-save-edit']");
    private final By quitAdding = By.xpath("//a[@class='icon-large icon-close dark-hover js-cancel-edit' and @href='#']");
    private final By showMenuLocator = By.xpath("a[@class='board-header-btn mod-show-menu js-show-sidebar']");
    private final By hideMenuLocator = By.xpath("a[@class='board-menu-header-close-button icon-lg icon-close js-hide-sidebar']");
    private final By genericListLocator = By.xpath("//div[@class='js-list list-wrapper']");
    private final By closeMenuLocator = By.xpath("//a[@title='Close the board menu.']");
    private final By titleLinkLocator = By.xpath("//a[@class='board-header-btn board-header-btn-name js-rename-board']");
    private final By newNameInputLocator = By.xpath("//input[@class='js-board-name js-autofocus']");
    private final By nameChangeButtonLocator = By.xpath("//input[@class='primary wide js-rename-board' and @value='Rename']");
    private final By bodyLocator = By.xpath("//body");
    private By workPageTitleLocator;
    private WebDriver driver;

    public WorkPageHTML(WebDriver driver, String workPageTitle) {
        super(driver);
        setWorkPageTitleLocator(workPageTitle);
    }

    public By getListTitleInputLocator() {
        return listTitleInputLocator;
    }

    public By getSaveListLocator() {
        return saveListLocator;
    }

    public By getQuitAdding() {
        return quitAdding;
    }

    public By getShowMenuLocator() {
        return showMenuLocator;
    }

    public By getHideMenuLocator() {
        return hideMenuLocator;
    }

    public By getGenericListLocator() {
        return genericListLocator;
    }

    public By getCloseMenuLocator() {
        return closeMenuLocator;
    }

    public By getTitleLinkLocator() {
        return titleLinkLocator;
    }

    public By getNewNameInputLocator() {
        return newNameInputLocator;
    }

    public By getNameChangeButtonLocator() {
        return nameChangeButtonLocator;
    }

    public By getListParentLocator(String listTitle) {
        return By.xpath("//div[@class='js-list list-wrapper' and .//textarea[text()='" + listTitle + "']]");
    }

    public By getWorkPageTitleLocator() {
        return workPageTitleLocator;
    }

    public By getWorkPageTitleLocator(String workPageTitle) {
        setWorkPageTitleLocator(workPageTitle);
        return workPageTitleLocator;
    }

    public By getBodyLocator() {
        return bodyLocator;
    }

    public void setWorkPageTitleLocator(String workPageTitle) {
        this.workPageTitleLocator = By.xpath("//span[@class='board-header-btn-text' and text()='" + workPageTitle + "']");
    }

    public PageElement getListTitleInput() {
        return getElement(getListTitleInputLocator());
    }

    public PageElement getSaveListButton() {
        return getElement(getSaveListLocator());
    }

    public PageElement getQuitAddingButton() {
        return getElement(getQuitAdding());
    }

    public PageElement getShowMenuLink() {
        return getElement(getShowMenuLocator());
    }

    public PageElement getHideMenuLink() {
        return getElement(getHideMenuLocator());
    }

    public PageElement getGenericList() {
        return getElement(getGenericListLocator());
    }

    public PageElement getCloseMenuButton() {
        return getElement(getCloseMenuLocator());
    }

    public PageElement getTitleLink() {
        return getElement(getTitleLinkLocator());
    }

    public PageElement getNewNameInput() {
        return getElement(getNewNameInputLocator());
    }

    public PageElement getNameChangeButton() {
        return getElement(getNameChangeButtonLocator());
    }

    public PageElement getBody() {
        return getElement(getBodyLocator());
    }
}
