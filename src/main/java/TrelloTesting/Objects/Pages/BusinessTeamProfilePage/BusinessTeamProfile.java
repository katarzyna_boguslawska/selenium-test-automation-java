package TrelloTesting.Objects.Pages.BusinessTeamProfilePage;

import TrelloTesting.Interfaces.Profilable;
import TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Boards.TeamBoards;
import TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Members.TeamMembers;
import TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.TeamSettings;
import TrelloTesting.FixedChoices.Visibilities;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import TrelloTesting.Objects.Pages.CommonHTML.TeamProfilePageHTML;
import org.openqa.selenium.WebDriver;

public class BusinessTeamProfile extends LoggedInPage implements Profilable {

    public WebDriver driver;
    private TeamSettings teamSettings;
    private TeamMembers teamMembers;
    private TeamBoards teamBoards;
    private TeamProfilePageHTML html;

    public String teamName;

    public BusinessTeamProfile(WebDriver driver, String teamName) {
        super(driver);
        this.driver = driver;
        this.teamName = teamName;
        this.teamSettings = new TeamSettings(this.driver, teamName);
        this.teamMembers = new TeamMembers(this.driver);
        this.teamBoards = new TeamBoards(this.driver);
        this.html = new TeamProfilePageHTML(driver);
    }

    @Override
    public TeamMembers goToMemebers() {
        html.getMembersTabButton().click();
        return teamMembers;
    }

    @Override
    public TeamSettings goToSettings() {
        html.getSettingsTabButton().click();
        return teamSettings;
    }

    @Override
    public TeamBoards goToBoards() {
        html.getBoardsTabButton().click();
        return teamBoards;
    }

    @Override
    public void addMember(String who) {
        goToMemebers().addMember(who);
    }

    @Override
    public void changeVisibility(Visibilities toWhatVisibility) {
        goToSettings().changeVisibility(toWhatVisibility);

    }

    @Override
    public void deleteTeam() {
        goToSettings().delete();
    }

    @Override
    public void removeMember() {
        html.getRemoveMemberButton().click();
        html.getConfirmMemberRemovalButton().click();
    }

    @Override
    public Boolean isNamed() {
        return isPresent(html.getTeamProfileHeaderLocator(teamName));
    }

    @Override
    public Boolean isPublicVisibilityDisplayed() {
        return teamSettings.getCurrentVisibility() == Visibilities.PUBLIC ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Boolean isPrivateVisibilityDisplayed() {
        return teamSettings.getCurrentVisibility() == Visibilities.PRIVATE ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public Boolean isMemberAdded(String who) {
        String username = "@" + who.substring(0, who.indexOf("@"));
        return isPresent(html.getNewMemberLocator(username));
    }
}
