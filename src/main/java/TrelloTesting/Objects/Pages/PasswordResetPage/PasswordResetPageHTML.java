package TrelloTesting.Objects.Pages.PasswordResetPage;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class PasswordResetPageHTML extends BasePage{

    private final By emailInputLocator = By.xpath("//input[@id='email']");
    private final By submitButtonLocator = By.xpath("//input[@id='submit']");
    private final By resetHeadingLocator = By.xpath("//h1");
    private final By resetDetailsLocator = By.xpath("//p[@id='feedback']");

    private final By tEmailInputLocator = By.xpath("//input[@id='forgot-email']");
    private final By tSubmitButtonLocator = By.xpath("//button[@class='button button-green js-forgot-button']");
    private final By tEmailNotificationLocator = By.xpath("//p[@class='js-check-email-text text-center']");
    private WebDriver driver;

    public PasswordResetPageHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getEmailInputLocator() {
        return emailInputLocator;
    }

    public By getSubmitButtonLocator() {
        return submitButtonLocator;
    }

    public By getResetHeadingLocator() {
        return resetHeadingLocator;
    }

    public By getResetDetailsLocator() {
        return resetDetailsLocator;
    }

    public By gettEmailInputLocator() {
        return tEmailInputLocator;
    }

    public By gettSubmitButtonLocator() {
        return tSubmitButtonLocator;
    }

    public By gettEmailNotificationLocator() {
        return tEmailNotificationLocator;
    }


    public PageElement getEmailInput() {
        return getElement(getEmailInputLocator());
    }

    public PageElement getSubmitButton() {
        return getElement(getSubmitButtonLocator());
    }

    public PageElement getResetHeading() {
        return getElement(getResetHeadingLocator());
    }

    public PageElement getResetDetails() {
        return getElement(getResetDetailsLocator());
    }

    public PageElement gettEmailInput() {
        return getElement(gettEmailInputLocator());
    }

    public PageElement gettSubmitButton() {
        return getElement(gettSubmitButtonLocator());
    }

    public PageElement gettEmailNotification() {
        return getElement(gettEmailNotificationLocator());
    }
}
