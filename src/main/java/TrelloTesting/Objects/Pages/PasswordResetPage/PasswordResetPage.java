package TrelloTesting.Objects.Pages.PasswordResetPage;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;


public class PasswordResetPage extends BasePage {

    private final String url;
    private PasswordResetPageHTML html;
    
    public PasswordResetPage(WebDriver driver){
        super(driver);
        url = "https://trello.com/forgot";
        html = new PasswordResetPageHTML(driver);
    }

    public Boolean isEmailSent(){
        Boolean isHeadingAvailable = isPresent(html.getResetHeadingLocator());
        Boolean isDetailsAvailable = isPresent(html.getResetDetailsLocator());
        Boolean isEmailSent = isPresent(html.gettEmailNotificationLocator());
        Boolean basicCondition = isHeadingAvailable & isDetailsAvailable;
        return basicCondition || isEmailSent ? Boolean.TRUE : Boolean.FALSE;
    }

    public void completeDataForReset(String login){
        try{
            PageElement emailInput = html.getEmailInput();
            emailInput.type(login);
            html.getSubmitButton().click();
        } catch (TimeoutException e){
            html.gettSubmitButton().click();
        }
    }
}
