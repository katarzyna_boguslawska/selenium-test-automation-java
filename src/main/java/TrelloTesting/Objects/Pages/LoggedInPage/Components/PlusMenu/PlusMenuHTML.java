package TrelloTesting.Objects.Pages.LoggedInPage.Components.PlusMenu;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class PlusMenuHTML extends BasePage{

    private final By createBoardLocator = By.xpath("//a@[class='js-new-board']");
    private final By createPersonalTeamLocator = By.xpath("//a[@class='js-new-org']");
    private final By createBusinessTeamLocator = By.xpath("//a[@class='js-new-bc-org']");
    private WebDriver driver;

    public PlusMenuHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getCreateBoardLocator() {
        return createBoardLocator;
    }

    public By getCreatePersonalTeamLocator() {
        return createPersonalTeamLocator;
    }

    public By getCreateBusinessTeamLocator() {
        return createBusinessTeamLocator;
    }

    public PageElement getCreateBoardOption() {
        return getElement(getCreateBoardLocator());
    }

    public PageElement getCreatePersonalTeamOption() {
        return getElement(getCreatePersonalTeamLocator());
    }

    public PageElement getCreateBusinessTeamOption() {
        return getElement(getCreateBusinessTeamLocator());
    }
}
