package TrelloTesting.Objects.Pages.LoggedInPage.Components.PlusMenu;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Pages.LoggedInPage.Components.PlusMenu.Popups.BusinessTeamCreator;
import TrelloTesting.Objects.Pages.LoggedInPage.Components.PlusMenu.Popups.PersonalTeamCreator;
import TrelloTesting.Objects.Pages.BusinessTeamProfilePage.BusinessTeamProfile;
import TrelloTesting.Objects.Pages.PersonalTeamProfilePage.PersonalTeamProfile;
import org.openqa.selenium.WebDriver;

public class PlusMenu extends BasePage {

    private WebDriver driver;
    private PlusMenuHTML html;

    public PlusMenu(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new PlusMenuHTML(driver);
    }

    private PersonalTeamCreator openPersonalTeamCreationMenu(){
        html.getCreatePersonalTeamOption().click();
        return new PersonalTeamCreator(driver);
    }

    private BusinessTeamCreator openBusinessTeamCreationMenu(){
        html.getCreateBusinessTeamOption().click();
        return new BusinessTeamCreator(driver);
    }

    public PersonalTeamProfile createPersonalTeam(String teamName){
        PersonalTeamCreator creator = openPersonalTeamCreationMenu();
        return creator.createTeam(teamName);
    }

    public PersonalTeamProfile createPersonalTeam(String teamName, String description){
        PersonalTeamCreator creator = openPersonalTeamCreationMenu();
        return creator.createTeam(teamName, description);
    }

    public BusinessTeamProfile createBusinessTeam(String name){
        BusinessTeamCreator creator = openBusinessTeamCreationMenu();
        return creator.createTeam(name);
    }

    public BusinessTeamProfile createBusinessTeam(String name, String description){
        BusinessTeamCreator creator = openBusinessTeamCreationMenu();
        return creator.createTeam(name, description);
    }
}

