package TrelloTesting.Objects.Pages.LoggedInPage.Components.UserMenu;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class UserMenuHTML extends BasePage {

    private final By profileLocator = By.xpath("/a[@class='js-profile']");
    private final By cardsLocator = By.xpath("//a[@class='js-cards']");
    private final By settingsLocator = By.xpath("//a[@class='js-account']");
    private final By helpLocator = By.xpath("//a[@class='js-help']");
    private final By shortcutsLocator = By.xpath("//a[@class='js-shortcuts']");
    private final By changeLanguageLocator = By.xpath("//a[@class='js-change-locale']");
    private final By logoutLocator = By.xpath("//a[@class='js-logout']");
    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private WebDriver driver;

    public UserMenuHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getProfileLocator() {
        return profileLocator;
    }

    public By getCardsLocator() {
        return cardsLocator;
    }

    public By getSettingsLocator() {
        return settingsLocator;
    }

    public By getHelpLocator() {
        return helpLocator;
    }

    public By getShortcutsLocator() {
        return shortcutsLocator;
    }

    public By getChangeLanguageLocator() {
        return changeLanguageLocator;
    }

    public By getLogoutLocator() {
        return logoutLocator;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public PageElement getProfileOption() {
        return getElement(getProfileLocator());
    }

    public PageElement getCardsOption() {
        return getElement(getCardsLocator());
    }

    public PageElement getSettingsOption() {
        return getElement(getSettingsLocator());
    }

    public PageElement getHelpOption() {
        return getElement(getHelpLocator());
    }

    public PageElement getShortcutsOption() {
        return getElement(getShortcutsLocator());
    }

    public PageElement getChangeLanguageOption() {
        return getElement(getChangeLanguageLocator());
    }

    public PageElement getLogoutOption() {
        return getElement(getLogoutLocator());
    }

    public PageElement getCloseOption() {
        return getElement(getCloseLocator());
    }
}
