package TrelloTesting.Objects.Pages.LoggedInPage.Components.UserMenu;

import TrelloTesting.Objects.Base.BasePage;
import org.openqa.selenium.WebDriver;
import TrelloTesting.Interfaces.Closeable;

public class UserMenu extends BasePage implements Closeable{

    private WebDriver driver;
    private UserMenuHTML html;

    public UserMenu(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new UserMenuHTML(driver);
    }

    public void close(){
        html.getCloseOption().click();
    }

    public void logout(){
        html.getLogoutOption().click();
    }
}
