package TrelloTesting.Objects.Pages.LoggedInPage.Components.PlusMenu.Popups;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Pages.BusinessTeamProfilePage.BusinessTeamProfile;
import TrelloTesting.Interfaces.Closeable;
import TrelloTesting.Interfaces.Creatorable;
import TrelloTesting.Objects.Pages.LoggedInPage.Components.PlusMenu.Popups.CreatorHTML;
import org.openqa.selenium.WebDriver;


public class BusinessTeamCreator  extends BasePage implements Creatorable, Closeable {


    private WebDriver driver;
    private CreatorHTML html;

    public BusinessTeamCreator(WebDriver driver) {
        super(driver);
        this.driver = driver;
        this.html = new CreatorHTML(driver);
    }

    @Override
    public void completeName(String name) {
        html.getGroupNameInput().type(name);
    }

    @Override
    public void completeDescription(String description) {
        html.getDescriptionInput().type(description);
    }

    @Override
    public BusinessTeamProfile clickCreate(String name) {
        html.getCreateButton().click();
        return new BusinessTeamProfile(driver, name);
    }

    @Override
    public BusinessTeamProfile createTeam(String name) {
        completeName(name);
        return clickCreate(name);
    }

    @Override
    public BusinessTeamProfile createTeam(String name, String description) {
        completeName(name);
        completeDescription(description);
        return clickCreate(name);
    }

    @Override
    public void close() {
        html.getCloseButton().click();
    }
}
