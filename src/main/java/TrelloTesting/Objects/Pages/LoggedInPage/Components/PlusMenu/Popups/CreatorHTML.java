package TrelloTesting.Objects.Pages.LoggedInPage.Components.PlusMenu.Popups;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class CreatorHTML extends BasePage{

    private final By groupNameLocator = By.xpath("//input[@id='org-display-name']");
    private final By descriptionLocator = By.xpath("//textarea[@id='org-desc']");
    private final By createButtonLocator = By.xpath("//input[@class='primary wide js-save' and @value='Create']");
    private final By closeLocator = By.xpath("//a[@class='pop-over-header-close-btn icon-sm icon-close']");
    private final By backLocator = By.xpath("//a[@class='pop-over-header-back-btn icon-sm icon-back is-shown']");
    private WebDriver driver;

    public CreatorHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getGroupNameLocator() {
        return groupNameLocator;
    }

    public By getDescriptionLocator() {
        return descriptionLocator;
    }

    public By getCreateButtonLocator() {
        return createButtonLocator;
    }

    public By getCloseLocator() {
        return closeLocator;
    }

    public By getBackLocator() {
        return backLocator;
    }

    public PageElement getGroupNameInput() {
        return getElement(getGroupNameLocator());
    }

    public PageElement getDescriptionInput() {
        return getElement(getDescriptionLocator());
    }

    public PageElement getCreateButton() {
        return getElement(getCreateButtonLocator());
    }

    public PageElement getCloseButton() {
        return getElement(getCloseLocator());
    }

    public PageElement getBackButton() {
        return getElement(getBackLocator());
    }
}
