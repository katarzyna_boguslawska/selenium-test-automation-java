package TrelloTesting.Objects.Pages.LoggedInPage;

import TrelloTesting.Interfaces.WorkPageable;
import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import TrelloTesting.Objects.Pages.LoggedInPage.Components.PlusMenu.PlusMenu;
import TrelloTesting.Objects.Pages.LoggedInPage.Components.UserMenu.UserMenu;
import TrelloTesting.Objects.Pages.BusinessTeamProfilePage.BusinessTeamProfile;
import TrelloTesting.Objects.Pages.PersonalTeamProfilePage.PersonalTeamProfile;
import TrelloTesting.Objects.Pages.GroupWorkPage.GroupWorkPage;
import TrelloTesting.Objects.Pages.PersonalWorkPage.PersonalWorkPage;
import TrelloTesting.Objects.Pages.SearchResultsPage.SearchResultsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class LoggedInPage extends BasePage {

    private WebDriver driver;
    public List <WorkPageable> boards;
    private LoggedInPageHTML html;

    public LoggedInPage(WebDriver driver){

        super(driver);
        this.driver = driver;
        this.boards = new ArrayList<>();
        this.html = new LoggedInPageHTML(driver);
    }

    public Boolean isCreateBoardPossible(){
        return isPresent(html.getCreateNewBoardLocator());
    }

    private By locateWorkspace(String workspaceTitle){
        List<PageElement> possibleParents = html.getPossibleParents(workspaceTitle);
        for (PageElement parent : possibleParents){
            String headerText = getElement(parent, html.getTileHeaderLocator()).getText();
            if (headerText.equals(workspaceTitle)){
                return parent.getLocator();
            }
        }
        throw new NoSuchElementException();
    }

    private GroupWorkPage makeNewGroupBoard(String groupName, String boardTitle){
        By workspaceLocator = locateWorkspace(groupName);
        getElement(workspaceLocator, html.getBoardRelativeXpath()).click();
        html.getBoardTitleInput().type(boardTitle);
        html.getCreateBoardButton().click();
        return new GroupWorkPage(driver, boardTitle, groupName);
    }

    public void addNewGroupBoard(String groupName, String boardTitle){
        GroupWorkPage groupWorkPage = makeNewGroupBoard(groupName, boardTitle);
        boards.add(groupWorkPage);
    }

    private PersonalWorkPage makeNewPersonalBoard(String boardTitle){
        By workspaceLocator = locateWorkspace("Personal Boards");
        getElement(workspaceLocator, html.getBoardRelativeXpath()).click();
        html.getBoardTitleInput().type(boardTitle);
        html.getCreateBoardButton().click();
        return new PersonalWorkPage(driver, boardTitle);
    }

    public void addNewPersonalBoard(String boardTitle){
        PersonalWorkPage personalWorkPage = makeNewPersonalBoard(boardTitle);
        boards.add(personalWorkPage);
    }

    public GroupWorkPage goToWorkPage(String groupName, String boardTitle){
        By boardButton = html.getBoardTileLocator(boardTitle);
        By workspaceLocator = locateWorkspace(groupName);
        getElement(workspaceLocator, boardButton).click();
        return new GroupWorkPage(driver, boardTitle, groupName);
    }

    public SearchResultsPage searchForBoard(String boardTitle){
        html.getBoardSearchInput().focusAndType(boardTitle);
        html.getSearchButton().click();
        return new SearchResultsPage(driver, boardTitle);
    }


    public LoggedInPage goToHomeDashboard(){
        html.getHomeDashboard().click();
        return new LoggedInPage(driver);
    }

    public PlusMenu showPlusMenu(){
        html.getPlusMenu().click();
        return new PlusMenu(driver);
    }

    public UserMenu showUserMenu(){
        html.getAvatarMenu().click();
        return new UserMenu(driver);
    }

    public PersonalTeamProfile createPersonalTeam(String groupName){
        PlusMenu menu = showPlusMenu();
        return menu.createPersonalTeam(groupName);
    }

    public BusinessTeamProfile createBusinessTeam(String groupName){
        PlusMenu menu = showPlusMenu();
        return menu.createBusinessTeam(groupName);
    }

    public void logOut(){
        UserMenu menu = showUserMenu();
        menu.logout();

    }
}
