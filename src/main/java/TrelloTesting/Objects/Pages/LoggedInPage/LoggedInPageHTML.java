package TrelloTesting.Objects.Pages.LoggedInPage;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import com.gargoylesoftware.htmlunit.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

public final class LoggedInPageHTML extends BasePage{

    private final By welcomeBoardLocator = By.xpath("//a[@class='board-tile' and contains(@href, 'welcome-board')]");
    private final By createNewBoardLocator = By.xpath("//a[@class='board-tile mod-add']");
    private final By boardTitleInputLocator = By.xpath("//input[@id='boardNewTitle']");
    private final By createBoardButton = By.xpath("//input[@class='primary wide js-submit' and @type='submit' and @value='Create']");
    private final By avatarMenuLocator = By.xpath("//a[@class='header-btn header-avatar js-open-header-member-menu']");
    private final By boardSearchInputLocator = By.xpath("//input[@class='header-search-input js-search-input js-disable-on-dialog']");
    private final By searchButtonLocator = By.xpath("//a[@class='header-search-icon header-search-icon-open icon-lg icon-external-link light js-open-button js-open-search-page header-search-icon-dark']");
    private final By homeDashboardLocator = By.xpath("//a[@class='header-logo js-home-via-logo' and @aria-label='Trello Home']");
    private final By plusMenuLocator = By.xpath("//a[@class='header-btn js-open-add-menu']");
    private final By boardRelativeXpath = By.xpath(".//a[@class='board-tile mod-add' and @href='#']");
    private final By tileHeaderLocator = By.xpath("./div/h3");

    private WebDriver driver;

    public LoggedInPageHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getWelcomeBoardLocator() {
        return welcomeBoardLocator;
    }

    public By getCreateNewBoardLocator() {
        return createNewBoardLocator;
    }

    public By getBoardTitleInputLocator() {
        return boardTitleInputLocator;
    }

    public By getCreateBoardButtonLocator() {
        return createBoardButton;
    }

    public By getAvatarMenuLocator() {
        return avatarMenuLocator;
    }

    public By getBoardSearchInputLocator() {
        return boardSearchInputLocator;
    }

    public By getSearchButtonLocator() {
        return searchButtonLocator;
    }

    public By getHomeDashboardLocator() {
        return homeDashboardLocator;
    }

    public By getPlusMenuLocator() {
        return plusMenuLocator;
    }

    public By getBoardRelativeXpath() {
        return boardRelativeXpath;
    }

    public By getBoardTileLocator(String boardTitle) {
        return By.xpath(".//a[@class='board-tile' and .//span[@title='" + boardTitle + "']]");
    }

    public By getPossibleParentsLocator(String boardTitle) {
        return By.xpath("//h3[@class='boards-page-board-section-header-name' and contains(text(),'" + boardTitle + "')]/../..");
    }

    public By getTileHeaderLocator() {
        return tileHeaderLocator;
    }

    public PageElement getWelcomeBoard() {
        return getElement(getWelcomeBoardLocator());
    }

    public PageElement getCreateNewBoard() {
        return getElement(getCreateNewBoardLocator());
    }

    public PageElement getBoardTitleInput() {
        return getElement(getBoardTitleInputLocator());
    }

    public PageElement getCreateBoardButton() {
        return getElement(getCreateBoardButtonLocator());
    }

    public PageElement getAvatarMenu() {
        return getElement(getAvatarMenuLocator());
    }

    public PageElement getBoardSearchInput() {
        return getElement(getBoardSearchInputLocator());
    }

    public PageElement getSearchButton() {
        return getElement(getSearchButtonLocator());
    }

    public PageElement getHomeDashboard() {
        return getElement(getHomeDashboardLocator());
    }

    public PageElement getPlusMenu() {
        return getElement(getPlusMenuLocator());
    }

    public PageElement getRelativeBoard() {
        return getElement(getBoardRelativeXpath());
    }

    public PageElement getBoardTile(String boardTitle) {
        return getElement(getBoardTileLocator(boardTitle));
    }

    public List<PageElement> getPossibleParents(String boardTitle) {
        return getElements(getPossibleParentsLocator(boardTitle));
    }

    public PageElement getTileHeader(){
        return getElement(getTileHeaderLocator());
    }
}
