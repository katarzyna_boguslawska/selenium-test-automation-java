package TrelloTesting.Objects.Pages.LoginPage;


import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public final class LoginPageHTML extends BasePage{

    private final By titleLocator = (By.xpath("//h1"));
    private final By usernameFieldLocator = (By.xpath("//input[@id='user']"));
    private final By passwordFieldLocator = (By.xpath("//input[@id='password']"));
    private final By loginButtonLocator = (By.xpath("//input[@id='login']"));
    private final By passwordResetLinkLocator = (By.xpath("//a[@href='/forgot']"));

    //locators applicable to the "new" login page (the one with Taco));
    private final By tUsernameFieldLocator = (By.xpath("//input[@id='login-identifier']"));
    private final By tConfirmLocator = (By.xpath("//button[@class='button button-green js-login-button']"));
    private final By tLoginButtonLocator = (By.xpath("//button[@class='button button-green js-password-button']"));
    private WebDriver driver;

    public LoginPageHTML(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public By getTitleLocator() {
        return titleLocator;
    }

    public By getUsernameFieldLocator() {
        return usernameFieldLocator;
    }

    public By getPasswordFieldLocator() {
        return passwordFieldLocator;
    }

    public By getLoginButtonLocator() {
        return loginButtonLocator;
    }

    public By getPasswordResetLinkLocator() {
        return passwordResetLinkLocator;
    }

    public By gettUsernameFieldLocator() {
        return tUsernameFieldLocator;
    }

    public By gettConfirmLocator() {
        return tConfirmLocator;
    }

    public By gettLoginButtonLocator() {
        return tLoginButtonLocator;
    }


    public PageElement getTitle() {
        return getElement(getTitleLocator());
    }

    public PageElement getUsernameField() {
        return getElement(getUsernameFieldLocator());
    }

    public PageElement getPasswordField() {
        return getElement(getPasswordFieldLocator());
    }

    public PageElement getLoginButton() {
        return getElement(getLoginButtonLocator());
    }

    public PageElement getPasswordResetLink() {
        return getElement(getPasswordResetLinkLocator());
    }

    public PageElement gettUsernameField() {
        return getElement(gettUsernameFieldLocator());
    }

    public PageElement gettConfirm() {
        return getElement(gettConfirmLocator());
    }

    public PageElement gettLoginButton() {
        return getElement(gettLoginButtonLocator());
    }
}
