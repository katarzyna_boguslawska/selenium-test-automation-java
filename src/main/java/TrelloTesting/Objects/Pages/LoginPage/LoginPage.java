package TrelloTesting.Objects.Pages.LoginPage;

import TrelloTesting.Objects.Base.BasePage;
import TrelloTesting.Objects.Base.PageElement;
import TrelloTesting.Objects.Pages.FailedLoginPage.FailedLoginPage;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import TrelloTesting.Objects.Pages.PasswordResetPage.PasswordResetPage;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    protected WebDriver driver;
    private String url;
    private LoginPageHTML html;

    public LoginPage(WebDriver driver){
        super(driver);
        this.driver = driver;
        url = "https://trello.com/login";
        navigate(url);
        this.html = new LoginPageHTML(driver);

    }

    public Boolean isTitleMatches(){
        String title = html.getTitle().getText();
        return title.equals("Log in to Trello") ? Boolean.TRUE : Boolean.FALSE;
    }

    public void completeLogin(String login){
        try{
            PageElement loginInput = html.getUsernameField();
            loginInput.type(login).clickEnter();
        } catch (TimeoutException e) {
            PageElement loginInput = html.gettUsernameField();
            if (isClickable(html.gettUsernameFieldLocator())){
                loginInput.focusAndType(login);
                PageElement validateLoginButton = html.gettConfirm();
                if (isClickable(html.gettUsernameFieldLocator())){
                    validateLoginButton.click();
                }
            }
        }
    }

    public void completePassword(String password){
        html.getPasswordField().focusAndType(password);
    }

    public LoggedInPage submit(){
        try{
            html.getLoginButton().click();
        } catch (TimeoutException e) {
            PageElement submitButton = html.gettLoginButton();
            validateElement(submitButton);
            submitButton.click();
        }
        return new LoggedInPage(driver);
    }

    public void completeWrongLogin(String wrongLogin, String password){
        try {
            PageElement loginInput = html.getUsernameField();
            loginInput.type(wrongLogin).clickEnter();
            completePassword(password);
        } catch (TimeoutException e) {
            PageElement loginInput = html.gettUsernameField();
            if (isClickable(html.gettUsernameFieldLocator())){
                loginInput.focusAndType(wrongLogin);
                PageElement validateLoginButton = html.gettConfirm();
                if (isClickable(html.gettUsernameFieldLocator())){
                    validateLoginButton.click();
                }
            }
        }
    }

    public FailedLoginPage failLogin(){
        try{
            html.getLoginButton().click();
        } catch (TimeoutException e){
            PageElement submitButton = html.gettLoginButton();
            validateElement(submitButton);
            submitButton.click();
        }
        return new FailedLoginPage(driver);
    }
    
    public LoggedInPage logIn(String login, String password){
        completeLogin(login);
        completePassword(password);
        submit();
        return new LoggedInPage(driver);
    }

    public PasswordResetPage requestPasswordReset(String login){
        try{
            html.gettUsernameField().focusAndType(login);
            html.gettConfirm().click();
        } catch (TimeoutException e) {
            isVisible(html.getPasswordResetLinkLocator());
        }
        html.getPasswordResetLink().click();
        return new PasswordResetPage(driver);
    }
}
