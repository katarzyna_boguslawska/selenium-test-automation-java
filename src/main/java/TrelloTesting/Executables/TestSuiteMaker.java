package TrelloTesting.Executables;

import TrelloTesting.Tests.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CardManipulationTests.class,
        AddListsAndCardsTests.class,
        BoardManipulationTests.class,
        JsonExportTests.class,
        LoginTests.class,
        WorkPageBackgroundManipulationTests.class,
        TeamManipulationTests.class})

public class TestSuiteMaker {
}
