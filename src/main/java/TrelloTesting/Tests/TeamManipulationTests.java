package TrelloTesting.Tests;

import TrelloTesting.FixedChoices.Visibilities;
import TrelloTesting.Objects.Pages.PersonalTeamProfilePage.PersonalTeamProfile;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import TrelloTesting.Objects.Pages.LoginPage.LoginPage;
import TrelloTesting.Utils.TestSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestName;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Properties;

public class TeamManipulationTests {

    private static ChromeDriver driver;
    private static Properties props;
    private static LoggedInPage loggedIn;
    private static PersonalTeamProfile personalTeam;
    private Logger logger = LogManager.getLogger();

    @Before
    public void setUp() {
        props = TestSettings.getProperties();
        driver = new ChromeDriver();
        LoginPage loginPage = new LoginPage(driver);
        loggedIn = loginPage.logIn(props.getProperty("validUsername"), props.getProperty("validPassword"));
    }

    @Rule
    public TestName name = new TestName();

    @Test
    public void createTeam(){
        logger.info("TEST STARTED: " + name.getMethodName());
        personalTeam = loggedIn.createPersonalTeam("testTeam");
        Assert.assertTrue(personalTeam.isNamed());
    }

    @Test
    public void changeVisibilityToPublic(){
        logger.info("TEST STARTED: " + name.getMethodName());
        personalTeam = loggedIn.createPersonalTeam("testTeam");
        personalTeam.changeVisibility(Visibilities.PUBLIC);
        Assert.assertTrue(personalTeam.isPublicVisibilityDisplayed());
        personalTeam.changeVisibility(Visibilities.PRIVATE);
    }

    @Test
    public void addMemberToTeam(){
        logger.info("TEST STARTED: " + name.getMethodName());
        String member = props.getProperty("testMember");
        personalTeam = loggedIn.createPersonalTeam("testTeam");
        personalTeam.addMember(member);
        Assert.assertTrue(personalTeam.isMemberAdded(member));
//        personalTeam.removeMember();
    }

    @After
    public void cleanUpAndExit(){
        personalTeam.deleteTeam();
        loggedIn.isCreateBoardPossible();
        loggedIn.logOut();
        driver.quit();
    }
}
