package TrelloTesting.Tests;

import TrelloTesting.FixedChoices.Colors;
import TrelloTesting.FixedChoices.Emojis;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.List;
import TrelloTesting.Objects.Pages.GroupWorkPage.GroupWorkPage;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import TrelloTesting.Objects.Pages.LoginPage.LoginPage;
import TrelloTesting.Utils.TestSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestName;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Properties;

public class CardManipulationTests {
    private static ChromeDriver driver;
    private static GroupWorkPage workpage;
    private static Properties props;
    private static List list1;
    private static List list2;
    private static List list3;
    private static List list4;
    private Logger logger = LogManager.getLogger();


    @BeforeClass
    public static void setUp(){
        props = TestSettings.getProperties();

        driver = new ChromeDriver();
        LoginPage loginPage = new LoginPage(driver);
        LoggedInPage loggedIn = loginPage.logIn(props.getProperty("validUsername"), props.getProperty("validPassword"));
        loggedIn.addNewGroupBoard(props.getProperty("userGroup"), "Test group board");
        workpage = (GroupWorkPage)loggedIn.boards.get(0);

        for (int i = 0; i < 4; i++) {
            String listTitle = "List " + Integer.toString((i + 1));
            workpage.addList(listTitle);
        }
        list1 = workpage.findListByTitle("List 1");
        list2 = workpage.findListByTitle("List 2");
        list3 = workpage.findListByTitle("List 3");
        list4 = workpage.findListByTitle("List 4");

        for (int i = 0; i < 6; i++){
            String cardTitle = "Card " + Integer.toString((i + 1)) + " on List 1";
            list1.addCard(cardTitle);
        }
        for (int i = 0; i < 3; i++){
            String cardTitle = "Card " + Integer.toString((i + 1)) + " on List 2";
            list2.addCard(cardTitle);
        }
        for (int i = 0; i < 4; i++){
            String cardTitle = "Card " + Integer.toString((i + 1)) + " on List 3";
            list3.addCard(cardTitle);
        }
        for (int i = 0; i < 3; i++){
            String cardTitle = "Card " + Integer.toString((i + 1)) + " on List 4";
            list4.addCard(cardTitle);
        }
    }

    @Rule
    public TestName name = new TestName();

    @Test
    public void addDueDate(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list1.addDueDateOnCardWithText("Card 1 on List 1", "10/28/2017");
        Assert.assertTrue(list1.wasDueDateAddedToCardWithText("Card 1 on List 1"));
    }

    @Test
    public void labelCard(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list3.labelCardWithText("Card 1 on List 3", Colors.ORANGE);
        Assert.assertTrue(list3.wasCardWithTextLabelled("Card 1 on List 3", Colors.ORANGE));
    }

    @Test
    public void assignToSomebody(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list1.assignMemberToCardWithText("Card 4 on List 1", "Python Wsb");
        Assert.assertTrue(list1.wasCardWithTextAssignedTo("Card 4 on List 1", "Python Wsb"));
    }

    @Test
    public void dragCard(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list1.dragCardWithTextInPlaceOfCardWithText("Card 6 on List 1", "Card 3 on List 2", list2);
        Assert.assertTrue(list1.wasCardWithTextDroppedInPlaceOfCardWithText("Card 6 on List 1", list2));
    }

    @Test
    public void commentCard(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list3.commentCardWithText("Card 3 on List 3", "Test comment #1");
        Assert.assertTrue(list3.wasCardWithTextCommented("Card 3 on List 3", "Test comment #1"));
    }

    @Test
    public void commentWithEmoji(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list4.commentCardWithTextAndAddEmoji("Card 1 on List 4", "FFS...", Emojis.FACEPALM);
        Assert.assertTrue(list4.wasCardWithTextCommented("Card 1 on List 4", "FFS...", Emojis.FACEPALM));
    }

    @Test
    public void addAttachmentFromLink(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list3.addAttachmentFromLinkToCardWithText("Card 4 on List 3", props.getProperty("attachmentLink"));
        Assert.assertTrue(list3.wasAttachmentAddedToCardWithText("Card 4 on List 3"));
    }

    @Test
    public void removeComment(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list4.commentCardWithText("Card 2 on List 4", "This will be gone");
        list4.deleteCommentFromCardWithText("Card 2 on List 4", "This will be gone");
        Assert.assertTrue(list4.wasCommentRemovedFromCardWithText("Card 2 on List 4", "This will be gone"));
    }

    @Test
    public void archiveCard(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list1.archiveCardWithText("Card 2 on List 1");
        Assert.assertTrue(list1.wasCardWithTextArchived("Card 2 on List 1"));
    }

    @Test
    public void addChecklist(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list1.addChecklistToCardWithText("Card 5 on List 1", "Urgent");
        Assert.assertTrue(list1.wasChecklistAddedToCardWithText("Card 5 on List 1", "Urgent"));
    }

    @Test
    public void addPopulatedChecklist(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list1.addChecklistToCardWithText("Card 3 on List 1", "Urgent", "Task #");
        Assert.assertTrue(list1.wasChecklistPopulatedOnCardWithText("Card 3 on List 1", "Urgent"));
    }

    @Test
    public void makeProgressWithChecklistItems(){
        logger.info("TEST STARTED: " + name.getMethodName());
        list3.addChecklistToCardWithText("Card 2 on List 3", "Urgent", "Task #");
        String [] tasks = {"Task #2", "Task #3"};
        list3.completeListedTasksOnCardWithText("Card 2 on List 3", "Urgent", tasks);
        Assert.assertTrue(list3.wereTasksCompleted("Card 2 on List 3", "Urgent", 2));
    }

    @AfterClass
    public static void cleanUpAndExit(){
        workpage.deleteBoard();
        workpage.logOut();
        driver.quit();
    }
}
