package TrelloTesting.Tests;

import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.List;
import TrelloTesting.Objects.Pages.GroupWorkPage.GroupWorkPage;
import TrelloTesting.Objects.Pages.JsonExportedPage.JsonExportedPage;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import TrelloTesting.Objects.Pages.LoginPage.LoginPage;
import TrelloTesting.Utils.TestSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestName;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Properties;


public class JsonExportTests {
    private static ChromeDriver driver;
    private static GroupWorkPage workpage;
    private static Properties props;
    private static List list1;
    private Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void setUp() {
        props = TestSettings.getProperties();

        driver = new ChromeDriver();
        LoginPage loginPage = new LoginPage(driver);
        LoggedInPage loggedIn = loginPage.logIn(props.getProperty("validUsername"), props.getProperty("validPassword"));
        loggedIn.addNewGroupBoard(props.getProperty("userGroup"), "Test group board");
        workpage = (GroupWorkPage)loggedIn.boards.get(0);
        workpage.addList("List 1");
        list1 = workpage.findListByTitle("List 1");
        for (int i = 0; i < 6; i++) {
            String cardTitle = "Card " + Integer.toString((i + 1)) + " on List 1";
            list1.addCard(cardTitle);
        }
    }

    @Rule
    public TestName name = new TestName();

    @Test
    public void exportToJson(){
        logger.info("TEST STARTED: " + name.getMethodName());
        JsonExportedPage jsonPage = list1.exportToJsonCardWithText("Card 3 on List 1");
        Assert.assertTrue(jsonPage.isJson());
        workpage = jsonPage.unexportFromJson(workpage);
    }

    @AfterClass
    public static void cleanUpAndExit(){
        workpage.deleteBoard();
        workpage.logOut();
        driver.quit();
    }
}
