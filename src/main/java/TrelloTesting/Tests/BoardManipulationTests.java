package TrelloTesting.Tests;

import TrelloTesting.Objects.Pages.DeleteWorkPagePage.DeletedWorkPagePage;
import TrelloTesting.Objects.Pages.GroupWorkPage.GroupWorkPage;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import TrelloTesting.Objects.Pages.LoginPage.LoginPage;
import TrelloTesting.Objects.Pages.SearchResultsPage.SearchResultsPage;
import TrelloTesting.Utils.TestSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestName;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Properties;

public class BoardManipulationTests {

    private static ChromeDriver driver;
    private static Properties props;
    private static LoggedInPage loggedIn;
    private Logger logger = LogManager.getLogger();

    @Before
    public void setUp() {
        props = TestSettings.getProperties();

        driver = new ChromeDriver();
        LoginPage loginPage = new LoginPage(driver);
        loggedIn = loginPage.logIn(props.getProperty("validUsername"), props.getProperty("validPassword"));
    }

    @Rule public TestName name = new TestName();

    @Test
    public void createWorkPage(){
        logger.info("TEST STARTED: " + name.getMethodName());
        loggedIn.addNewGroupBoard(props.getProperty("userGroup"), "New Board");
        GroupWorkPage workpage = (GroupWorkPage)loggedIn.boards.get(0);
        Assert.assertTrue(workpage.isTitled("New Board"));
        workpage.deleteBoard();
    }

    @Test
    public void renameWorkPage(){
        logger.info("TEST STARTED: " + name.getMethodName());
        loggedIn.addNewGroupBoard(props.getProperty("userGroup"), "New Board");
        GroupWorkPage workpage = (GroupWorkPage)loggedIn.boards.get(0);
        workpage.renameBoard("Newly Named Board");
        Assert.assertTrue(workpage.isTitled("Newly Named Board"));
        workpage.deleteBoard();
    }

    @Test
    public void searchForWorkPage(){
        logger.info("TEST STARTED: " + name.getMethodName());
        loggedIn.addNewGroupBoard(props.getProperty("userGroup"), "New Board");
        GroupWorkPage workpage = (GroupWorkPage)loggedIn.boards.get(0);
        SearchResultsPage results = workpage.searchForBoard("New Board");
        Assert.assertTrue(results.isResultPresent());
        results.goToHomeDashboard().goToWorkPage(props.getProperty("userGroup"), "New Board").deleteBoard();
    }

    @Test
    public void deleteWorkPage() {
        logger.info("TEST STARTED: " + name.getMethodName());
        loggedIn.addNewGroupBoard(props.getProperty("userGroup"), "New Board");
        GroupWorkPage workpage = (GroupWorkPage)loggedIn.boards.get(0);
        DeletedWorkPagePage deletedBoardPage = workpage.deleteBoard();
        Assert.assertTrue(deletedBoardPage.isDeleted());
    }

    @After
    public void cleanUpAndExit(){
        loggedIn.logOut();
        driver.quit();
    }
}
