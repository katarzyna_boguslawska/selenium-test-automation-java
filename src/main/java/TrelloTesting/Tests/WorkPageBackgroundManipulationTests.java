package TrelloTesting.Tests;

import TrelloTesting.FixedChoices.Colors;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.BoardMenu;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import TrelloTesting.Objects.Pages.LoginPage.LoginPage;
import TrelloTesting.Objects.Pages.PersonalWorkPage.PersonalWorkPage;
import TrelloTesting.Utils.TestSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestName;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Properties;


public class WorkPageBackgroundManipulationTests {
    private static ChromeDriver driver;
    private static Properties props;
    private static LoggedInPage loggedPage;
    private static PersonalWorkPage personalWorkPage;
    private Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void openBrowser(){
        props = TestSettings.getProperties();
        driver = new ChromeDriver();
        LoginPage loginPage = new LoginPage(driver);
        String login = props.getProperty("validUsername");
        String password = props.getProperty("validPassword");
        loggedPage = loginPage.logIn(login, password);
        loggedPage.addNewPersonalBoard("Testing 1");
        personalWorkPage = (PersonalWorkPage)loggedPage.boards.get(0);
    }

    @Rule
    public TestName name = new TestName();

    @Test
    public void changeBackgroundToColor(){
        logger.info("TEST STARTED: " + name.getMethodName());
        BoardMenu menu = personalWorkPage.showBoardMenu();
        menu.changeBackgroundToColor(Colors.PISTACHIO);
        Assert.assertTrue(personalWorkPage.isBackgroundColored(Colors.PISTACHIO));
    }

    @Test
    public void changeBackgroundToPhoto(){
        logger.info("TEST STARTED: " + name.getMethodName());
        BoardMenu menu = personalWorkPage.showBoardMenu();
        menu.changeBackgroundToRandomPhoto();
        Assert.assertTrue(personalWorkPage.isBackgroundImage());
    }

    @After
    public void returnToBasicMenu(){
        BoardMenu menu = personalWorkPage.showBoardMenu();
        menu.returnToMainMenu();
    }

    @AfterClass
    public static void logOut(){
        personalWorkPage.deleteBoard();
        loggedPage.logOut();
        driver.quit();
    }
}
