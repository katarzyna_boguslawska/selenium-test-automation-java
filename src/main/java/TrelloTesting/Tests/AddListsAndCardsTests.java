package TrelloTesting.Tests;

import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.List;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import TrelloTesting.Objects.Pages.LoginPage.LoginPage;
import TrelloTesting.Objects.Pages.GroupWorkPage.GroupWorkPage;
import TrelloTesting.Utils.TestSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestName;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Properties;


public class AddListsAndCardsTests {
    private static ChromeDriver driver;
    private static Properties props;
    private static LoggedInPage loggedPage;
    private static GroupWorkPage groupWorkPage;
    private Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void getProperties(){
        props = TestSettings.getProperties();
    }

    @Before
    public void openBrowser(){
        driver = new ChromeDriver();
        LoginPage loginPage = new LoginPage(driver);
        String login = props.getProperty("validUsername");
        String password = props.getProperty("validPassword");
        loggedPage = loginPage.logIn(login, password);
        loggedPage.addNewGroupBoard("test_users", "Testing 1");
        groupWorkPage = (GroupWorkPage)loggedPage.boards.get(0);
    }

    @Rule public TestName name = new TestName();

    @Test
    public void addLists(){
        logger.info("TEST STARTED: " + name.getMethodName());
        groupWorkPage.addList("List 1");
        groupWorkPage.addList("List 2");
        Assert.assertEquals((long) groupWorkPage.countLists(), 2);
    }

    @Test
    public void addCards(){
        logger.info("TEST STARTED: " + name.getMethodName());
        groupWorkPage.addList("List 1");
        List list1 = groupWorkPage.findListByTitle("List 1");
        for (int i = 0; i < 4; i++){
            String cardTitle = "Card " + Integer.toString((i + 1)) + " on List 1";
            list1.addCard(cardTitle);
        }
    }

    @After
    public void logOut(){
        groupWorkPage.deleteBoard();
        loggedPage.logOut();
        driver.quit();
    }
}
