package TrelloTesting.Tests;

import TrelloTesting.Objects.Pages.FailedLoginPage.FailedLoginPage;
import TrelloTesting.Objects.Pages.LoggedInPage.LoggedInPage;
import TrelloTesting.Objects.Pages.LoginPage.LoginPage;
import TrelloTesting.Objects.Pages.PasswordResetPage.PasswordResetPage;
import TrelloTesting.Utils.TestSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.rules.TestName;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.Properties;


public class LoginTests {
    private static ChromeDriver driver;
    private static LoginPage loginPage;
    private static Properties props;
    private Logger logger = LogManager.getLogger();

    @BeforeClass
    public static void getProperties(){
        props = TestSettings.getProperties();
    }

    @Before
    public void openBrowser(){
        driver = new ChromeDriver();
        loginPage = new LoginPage(driver);
    }
    @Rule public TestName name = new TestName();

    @Test
    public void successfulLogin(){
        logger.info("TEST STARTED: " + name.getMethodName());
        String login = props.getProperty("validUsername");
        String password = props.getProperty("validPassword");
        LoggedInPage loggedPage = loginPage.logIn(login, password);
        Assert.assertTrue(loggedPage.isCreateBoardPossible());
        loggedPage.logOut();
    }

    @Test
    public void failLoginByPassword(){
        logger.info("TEST STARTED: " + name.getMethodName());
        String login = props.getProperty("validUsername");
        String badPassword = props.getProperty("invalidPassword");
        loginPage.completeLogin(login);
        loginPage.completePassword(badPassword);
        FailedLoginPage failurePage = loginPage.failLogin();
        Assert.assertTrue(failurePage.isErrorCommunicated());
    }

    @Test
    public void failLoginByUsername(){
        logger.info("TEST STARTED: " + name.getMethodName());
        String badLogin = props.getProperty("invalidUsername");
        String password = props.getProperty("validPassword");
        loginPage.completeWrongLogin(badLogin, password);
        FailedLoginPage failurePage = loginPage.failLogin();
        Assert.assertTrue(failurePage.isErrorCommunicated());
    }

    @Test
    public void resetPassword(){
        logger.info("TEST STARTED: " + name.getMethodName());
        String login = props.getProperty("validUsername");
        PasswordResetPage passwordResetPage = loginPage.requestPasswordReset(login);
        passwordResetPage.completeDataForReset(login);
        Assert.assertTrue(passwordResetPage.isEmailSent());

    }

    @After
    public void closeBrowser(){
        driver.quit();
    }
}
