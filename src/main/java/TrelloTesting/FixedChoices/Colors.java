package TrelloTesting.FixedChoices;


import org.openqa.selenium.By;

public enum Colors {

    GREEN (By.xpath("//span[@class='card-label mod-selectable card-label-green  js-select-label']"),
            By.xpath(".//span[@class='card-label mod-edit-label mod-clickable card-label-green palette-color js-palette-color']"),
            By.xpath("//div[@class='image' and @style='background-color: rgb(81, 152, 57);']"),
            By.xpath(".//span[@class='card-label card-label-green mod-card-front']"),
                    "rgba(81, 152, 57, 1)"),
    YELLOW(By.xpath("//span[@class='card-label mod-selectable card-label-yellow  js-select-label']"),
            By.xpath(".//span[@class='card-label mod-edit-label mod-clickable card-label-yellow palette-color js-palette-color']"),
            By.xpath("no such color"),
            By.xpath(".//span[@class='card-label card-label-yellow mod-card-front']"),
            "rgba(242, 214, 0, 1)"),
    ORANGE(By.xpath("//span[@class='card-label mod-selectable card-label-orange  js-select-label']"),
            By.xpath(".//span[@class='card-label mod-edit-label mod-clickable card-label-orange palette-color js-palette-color']"),
            By.xpath("//div[@class='image' and @style='background-color: rgb(210, 144, 52);']"),
            By.xpath(".//span[@class='card-label card-label-orange mod-card-front']"),
                    "rgba(210, 144, 52, 1)"),
    RED(By.xpath("//span[@class='card-label mod-selectable card-label-red  js-select-label']"),
            By.xpath(".//span[@class='class='card-label mod-edit-label mod-clickable card-label-red palette-color js-palette-color']"),
            By.xpath("//div[@class='image' and @style='background-color: rgb(176, 50, 70);']"),
            By.xpath(".//span[@class='card-label card-label-red mod-card-front']"),
                    "rgba(176, 50, 70, 1)"),
    PURPLE(By.xpath("//span[@class='card-label mod-selectable card-label-purple  js-select-label']"),
            By.xpath(".//span[@class='card-label mod-edit-label mod-clickable card-label-purple palette-color js-palette-color']"),
            By.xpath("//div[@class='image' and @style='background-color: rgb(137, 96, 158);']"),
            By.xpath(".//span[@class='card-label card-label-purple mod-card-front']"),
                    "rgba(137, 96, 158, 1)"),
    BLUE(By.xpath("//span[@class='card-label mod-selectable card-label-blue  js-select-label']"),
            By.xpath(".//span[@class='card-label mod-edit-label mod-clickable card-label-blue palette-color js-palette-color']"),
            By.xpath("//div[@class='image' and @style='background-color: rgb(0, 121, 191);']"),
            By.xpath(".//span[@class='card-label card-label-blue mod-card-front']"),
                    "rgba(0, 121, 191, 1)"),
    TURQUOISE(By.xpath(".//span[@class='card-label mod-edit-label mod-clickable card-label-sky palette-color js-palette-color']"),
            By.xpath("//div[@class='image' and @style='background-color: rgb(0, 174, 204);']"),
            By.xpath(".//span[@class='card-label card-label-sky mod-card-front']"),
                    "rgba(0, 174, 204, 1)"),
    PISTACHIO(By.xpath(".//span[@class='card-label mod-edit-label mod-clickable card-label-lime palette-color js-palette-color']"),
            By.xpath("//div[@class='image' and @style='background-color: rgb(75, 191, 107);']"),
            By.xpath(".//span[@class='card-label card-label-lime mod-card-front']"),
                    "rgba(75, 191, 107, 1)"),
    PINK(By.xpath(".//span[@class='card-label mod-edit-label mod-clickable card-label-pink palette-color js-palette-color']"),
            By.xpath("//div[@class='image' and @style='background-color: rgb(205, 90, 145);']"),
            By.xpath(".//span[@class='card-label card-label-pink mod-card-front']"),
                    "rgba(205, 90, 145, 1)"),
    GRAY(By.xpath(".//span[@class='card-label mod-edit-label mod-clickable card-label-black palette-color js-palette-color']"),
            By.xpath("//div[@class='image' and @style='background-color: rgb(131, 140, 145);']"),
            By.xpath(".//span[@class='card-label card-label-black mod-card-front']"),
                    "rgba(131, 140, 145, 1)");

    private final By labelLocator;
    private final By editLabelLocator;
    private final By backgroundLocator;
    private final By checkLabelLocator;
    private final String RGBcomponents;

    Colors(By labelLocator, By editLabelLocator, By backgroundLocator, By checkLabelLocator, String RGBcomponents){
        this.labelLocator = labelLocator;
        this.editLabelLocator = editLabelLocator;
        this.backgroundLocator = backgroundLocator;
        this.checkLabelLocator = checkLabelLocator;
        this.RGBcomponents = RGBcomponents;
    }

    Colors(By editLabelLocator, By backgroundLocator, By checkLabelLocator, String RGBcomponents){
        this.labelLocator = By.xpath("no such color");
        this.editLabelLocator = editLabelLocator;
        this.backgroundLocator = backgroundLocator;
        this.checkLabelLocator = checkLabelLocator;
        this.RGBcomponents = RGBcomponents;
    }

    public By getLabelLocator() {
        return labelLocator;
    }

    public By getEditLabelLocator() {
        return editLabelLocator;
    }

    public By getBackgroundLocator() {
        return backgroundLocator;
    }

    public By getCheckLabelLocator() {
        return checkLabelLocator;
    }

    public String getRGBcomponents() {
        return RGBcomponents;
    }
}
