package TrelloTesting.FixedChoices;

import org.openqa.selenium.By;

public enum BoardMenuSubmenus {
    MORE(By.xpath("//a[@class='board-menu-navigation-item-link js-open-more']")),
    BACKGROUND(By.xpath("//a[@class ='board-menu-navigation-item-link js-change-background']")),
    STICKERS(By.xpath("//a[@class='board-menu-navigation-item-link js-open-stickers']")),
    POWERUPS(By.xpath("//a[@class='board-menu-navigation-item-link js-open-power-ups']")),
    FILTER(By.xpath("//a[@class='board-menu-navigation-item-link js-open-card-filter']"));

    private final By submenuPositionLocator;

    BoardMenuSubmenus(By submenuPositionLocator){
        this.submenuPositionLocator = submenuPositionLocator;
    }

    public By getSubmenuPositionLocator() {
        return submenuPositionLocator;
    }
}
