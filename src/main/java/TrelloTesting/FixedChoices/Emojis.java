package TrelloTesting.FixedChoices;


import org.openqa.selenium.By;

public enum Emojis {
    THUMBSUP(By.xpath("//a[@class='name js-select-emoji' and @href='#' and @title='thumbsup']"),
            By.xpath("//img[@title='thumbsup' and @class='emoji']")),
    SMILE(By.xpath("//a[@class='name js-select-emoji' and @href='#' and @title='smile']"),
            By.xpath("//img[@title='smile' and @class='emoji']")),
    WARNING(By.xpath("//a[@class='name js-select-emoji' and @href='#' and @title='warning']"),
            By.xpath("//img[@title='warning' and @class='emoji']")),
    SUNGLASSES(By.xpath("//a[@class='name js-select-emoji' and @href='#' and @title='sunglasses']"),
            By.xpath("//img[@title='sunglasses' and @class='emoji']")),
    BALLOT(By.xpath("//a[@class='name js-select-emoji' and @href='#' and @title='ballot_box_with_check']"),
            By.xpath("//img[@title='ballot_box_with_check' and @class='emoji']")),
    FACEPALM(By.xpath("//a[@class='name js-select-emoji' and @href='#' and @title='facepalm']"),
            By.xpath("//img[@title='facepalm' and @class='emoji']"));

    private final By emojiLocator;
    private final By emojiImage;

    Emojis(By emojiLocator, By emojiImage){
        this.emojiLocator = emojiLocator;
        this.emojiImage = emojiImage;
    }

    public By getEmojiLocator() {
        return emojiLocator;
    }

    public By getEmojiImage() {
        return emojiImage;
    }
}