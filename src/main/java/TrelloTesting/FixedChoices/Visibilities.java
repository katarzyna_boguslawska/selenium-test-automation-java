package TrelloTesting.FixedChoices;


import org.openqa.selenium.By;

public enum Visibilities {
    PRIVATE(By.xpath("//span[text()='Private']"), By.xpath("//a[@class='highlight-icon js-select-members']")),
    PUBLIC(By.xpath("//span[text()='Public']"), By.xpath("//a[@class='highlight-icon js-select-public']"));

    private final By currentVisibilityLocator;
    private final By visibilityButtonLocator;

    Visibilities(By currentVisibilityLocator, By visibilityButtonLocator){
        this.currentVisibilityLocator = currentVisibilityLocator;
        this.visibilityButtonLocator = visibilityButtonLocator;
    }

    public By getCurrentVisibilityLocator() {
        return currentVisibilityLocator;
    }

    public By getVisibilityButtonLocator() {
        return visibilityButtonLocator;
    }
}
