package TrelloTesting.Interfaces;


import TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Boards.TeamBoards;
import TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Members.TeamMembers;
import TrelloTesting.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.TeamSettings;
import TrelloTesting.FixedChoices.Visibilities;

public interface Profilable {

    TeamMembers goToMemebers();
    TeamSettings goToSettings();
    TeamBoards goToBoards();
    void addMember(String who);
    void changeVisibility(Visibilities toWhatVisibility);
    void deleteTeam();
    void removeMember();
    Boolean isNamed();
    Boolean isPublicVisibilityDisplayed();
    Boolean isPrivateVisibilityDisplayed();
    Boolean isMemberAdded(String who);
}
