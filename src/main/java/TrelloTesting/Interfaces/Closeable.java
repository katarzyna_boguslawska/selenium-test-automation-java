package TrelloTesting.Interfaces;


public interface Closeable {
    void close();
}
