package TrelloTesting.Interfaces;


public interface Deletable {
    void delete();
}
