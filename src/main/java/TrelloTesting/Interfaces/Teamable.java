package TrelloTesting.Interfaces;

public interface Teamable {
    void completeName(String name);
    void completeDescription(String description);
    Profilable create(String name);
    Profilable create(String name, String description);
}
