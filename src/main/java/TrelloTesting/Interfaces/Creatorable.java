package TrelloTesting.Interfaces;

public interface Creatorable {
    void completeName(String name);
    void completeDescription(String description);
    Profilable clickCreate(String name);
    Profilable createTeam(String name);
    Profilable createTeam(String name, String description);
}
