package TrelloTesting.Interfaces;


import TrelloTesting.FixedChoices.Colors;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.List.List;
import TrelloTesting.Objects.Pages.CommonComponents.WorkpageComponents.Menu.BoardMenu;
import TrelloTesting.Objects.Pages.DeleteWorkPagePage.DeletedWorkPagePage;

public interface WorkPageable {
    void addList(String listTitle);
    List findListByTitle(String listTitle);
    void archiveListWithTitle(String listTitle);
    int countLists();
    BoardMenu showBoardMenu();
    DeletedWorkPagePage deleteBoard();
    void renameBoard(String newName);
    Boolean isTitled(String name);
    Boolean isBackgroundColored(Colors color);
    Boolean isBackgroundImage();
}
