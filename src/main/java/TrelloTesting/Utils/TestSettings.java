package TrelloTesting.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;


public class TestSettings {

    private static File file = new File("/home/kasia/IdeaProjects/Trello/src/main/java/TrelloTesting/Utils/test.properties");
    private static FileInputStream fileInput = null;

    public static Properties getProperties() {
        try {
            fileInput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties prop = new Properties();
        try {
            prop.load(fileInput);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
}
